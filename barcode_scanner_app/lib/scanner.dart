import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'constants.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'dart:developer';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'constants.dart';
import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'dart:convert';

class scanner extends StatefulWidget {
  scanner({Key? key}) : super(key: key);
  @override
  _scannerState createState() => _scannerState();
}

class _scannerState extends State<scanner> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  Barcode? barcode;
  QRViewController? controller;
  bool is_data = false;
  final storage = new FlutterSecureStorage();
  dynamic total_points;
  final today = DateTime.now();
  bool? isExpired;

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  void reassemble() async {
    super.reassemble();
    if (Platform.isAndroid) {
      await controller!.pauseCamera();
    }
    controller!.resumeCamera();
  }

  Future getUserData() async {
    String? value = await storage.read(key: constants.storage_key);
    String valuekey = value!;
    print(valuekey);
    Map<String, dynamic> decodedToken = JwtDecoder.decode(valuekey);
    print(decodedToken["_id"]);
    print(decodedToken);
    return decodedToken["_id"].toString();
  }

  Future getQRData(String id) async {
    var url = Uri.parse(constants.API_URL + 'qr-code/' + id);
    var token = await storage.read(key: 'lokintoken');
    // print("id --------->" + id.toString());
    // print('here: ' + token.toString());
    var res = await http.get(url, headers: <String, String>{
      'Context-Type': 'application/json;charSet=UTF-8',
      'authorization': token.toString()
    });
    if (res.statusCode == 200) {
      Map<String, dynamic> jsonresponse = json.decode(res.body);
      print("Json data is,");
      print(jsonresponse);
      setState(() {
        this.is_data = true;
      });
      return jsonresponse;
    } else {
      setState(() {
        this.is_data = false;
      });
      return false;
    }
  }

  Future UpdateQRData(String id, String userId) async {
    var url = Uri.parse(constants.API_URL + 'updateqrcode/');
    var token = await storage.read(key: 'lokintoken');
    print("id --------->" + id.toString());
    print('userId---------->: ' + userId);
    var res = await http.post(url, headers: <String, String>{
      'Context-Type': 'application/json;charSet=UTF-8',
      'authorization': token.toString(),
    }, body: <String, String>{
      'id': id.toString(),
      'user_id': userId
    });
    if (res.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future getConfigData() async {
    var url = Uri.parse(constants.API_URL + 'configData/');
    var token = await storage.read(key: 'lokintoken');
    var res = await http.get(url, headers: <String, String>{
      'Context-Type': 'application/json;charSet=UTF-8',
      'authorization': token.toString()
    });
    if (res.statusCode == 200) {
      Map<String, dynamic> jsonresponse = json.decode(res.body);
      print('Config data is + ');
      print(jsonresponse);
      return jsonresponse['qrexpire'];
    } else {
      return -1;
    }
  }

  // Future UpdateUserPoint(String userId, int updatedPoint) async {
  //   print("entered");
  //   var url = Uri.parse(constants.API_URL + 'updatepoint');
  //   String? value = await storage.read(key: constants.storage_key);
  //   String valuekey = value!;
  //   print(valuekey);
  //   var res = await http.post(url, headers: <String, String>{
  //     'Context-Type': 'application/json;charSet=UTF-8',
  //     'authorization': valuekey,
  //   }, body: {
  //     '_id': userId,
  //     'updatedPoint': updatedPoint,
  //   });
  //   print('Point updated');
  //   print(res.body);
  //   print(res.statusCode);
  // }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          alignment: Alignment.center,
          children: [
            buildQrView(context),
            Positioned(bottom: 15, child: displayResult()),
            Positioned(top: 10, child: controlButtens())
          ],
        ),
      ),
    );
  }

  Widget controlButtens() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8), color: Colors.white),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          IconButton(
            onPressed: () async {
              await controller?.toggleFlash();
              setState(() {});
            },
            icon: FutureBuilder(
                future: controller?.getFlashStatus(),
                builder: (context, snapshot) {
                  if (snapshot.data != null) {
                    return Icon(
                        snapshot.data == true
                            ? Icons.flash_on
                            : Icons.flash_off,
                        color:
                            snapshot.data == true ? Colors.red : Colors.black);
                  } else {
                    return Container();
                  }
                }),
          ),
          IconButton(
              onPressed: () async {
                await controller?.flipCamera();
                setState(() {});
              },
              icon: FutureBuilder(
                future: controller?.getCameraInfo(),
                builder: (context, snapshot) {
                  if (snapshot.data != null) {
                    return Icon(Icons.switch_camera,
                        color:
                            snapshot.data == true ? Colors.red : Colors.black);
                  } else {
                    print('change cammera');
                    return Container();
                  }
                },
              )),
        ],
      ),
    );
  }

  Widget displayResult() {
    return Container(
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10), color: Colors.white),
        child: Text(
          "scan a code!",
          maxLines: 3,
        ));
  }

  Widget buildQrView(BuildContext context) {
    return QRView(
      key: qrKey,
      onQRViewCreated: onQRViewCreated,
      overlay: QrScannerOverlayShape(
          cutOutSize: MediaQuery.of(context).size.width * 0.8,
          borderWidth: 10,
          borderLength: 40,
          borderRadius: 20,
          borderColor: constants.AppMainColor),
    );
  }

  void onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) async {
      controller.pauseCamera();
      if (scanData.code == null) {
        print('loading............');
        controller.resumeCamera();
      } else {
        var data = await getQRData(scanData.code);
        var userId = await getUserData();
        var expiryData = await getConfigData();

        print(expiryData);
        // var expiry_data =  {"flag":true,"qrexpire":0};
        var expiryDay = expiryData;
        print(data['CreatedOn']);
        // var CreatedOndate = int.parse(data['CreatedOn']);
        // print(data['CreatedOn']);
        var createdDate = DateTime.parse(data['CreatedOn']);
        print(createdDate);
        var expiryDate = createdDate.add(Duration(days: expiryDay));
        print(expiryDate);
        // bool isExpired = expiryDate.isAfter(today);
        // print(isExpired);

        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Text("Redeem your Voucher",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20)),
                      SizedBox(
                        height: 20,
                      ),
                      Text('QRCode: ' + data['QRCode'].toString()),
                      SizedBox(
                        height: 20,
                      ),
                      Text('Points: ' + data['Points'].toString()),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          MaterialButton(
                            height: 50.0,
                            minWidth: 80.0,
                            color: constants.AppMainColor,
                            textColor: Colors.white,
                            child: new Text("Close"),
                            onPressed: () => {Navigator.of(context).pop()},
                            splashColor: Colors.redAccent,
                          ),
                          MaterialButton(
                            height: 50.0,
                            minWidth: 80.0,
                            color: constants.AppMainColor,
                            textColor: Colors.white,
                            child: new Text("Redeem Point"),
                            onPressed: () => {
                              // Navigator.of(context).pop(),
                              // print('QR data is --------------------->'),
                              // print(data),
                              // print('is ScannedBy exist --------------------->'),
                              // print(data.containsKey('ScannedBy')),
                              if (data.containsKey('ScannedBy') == true)
                                {
                                  print('already scanned'),
                                  _onAlertWithCustomImageNotProcess(context)
                                }
                              else
                                {
                                  // if (data.containsKey('TotalPoint') == true)
                                  //   {
                                  //     total_points =
                                  //         data['TotalPoint'] + data['Points']
                                  //   }
                                  // else
                                  //   {
                                  //     total_points = data['Points'],
                                  //   },
                                  // print("----total point------->"),
                                  // print(total_points),
                                  // print('user id ------------->'),
                                  // print(userId),
                                  // UpdateUserPoint(
                                  //     user_id.toString(), total_points),
                                  isExpired = expiryDate.isAfter(today),
                                  print('isExpired----------->'),
                                  print(isExpired),
                                  if (isExpired == true)
                                    {
                                      print('QR is not expired'),
                                      UpdateQRData(data['QRCode'].toString(),
                                          userId.toString()),
                                      _onAlertWithCustomImagePressed(context),
                                    }
                                  else
                                    {
                                      print('QR is expired'),
                                      _PointExpired(context)
                                    }
                                }
                            },
                            splashColor: Colors.redAccent,
                          )
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      )
                    ],
                  ),
                ));
          },
        ).then((value) => controller.resumeCamera());
      }
    });
  }

  _onAlertWithCustomImagePressed(context) {
    Alert(
      context: context,
      title: "Successfully Added",
      desc: "Point is added to your Wallet",
      image: Image.asset("images/success.png"),
    ).show();
  }

  _onAlertWithCustomImageNotProcess(context) {
    Alert(
      context: context,
      title: "QR Code is Used",
      desc: "QR is alread user,Scan new QR",
      image: Image.asset("images/alert.png"),
    ).show();
  }

  _PointExpired(context) {
    Alert(
      context: context,
      title: "Expired!!!",
      desc: "QR Code is Expired",
      image: Image.asset("images/alert.png"),
    ).show();
  }

  _ServerError(context) {
    Alert(
      context: context,
      title: "Server Error!!!",
      desc: "Server is down",
      image: Image.asset("images/alert.png"),
    ).show();
  }
}
