import 'package:flutter/material.dart';
import 'constants.dart';
import 'package:dotted_decoration/dotted_decoration.dart';
import 'package:http/http.dart' as http;
import 'package:new_gradient_app_bar/new_gradient_app_bar.dart';
import 'package:FYD/userLoginMobile.dart';

import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:fluttertoast/fluttertoast.dart';

class notifications extends StatefulWidget {
  notifications({Key? key}) : super(key: key);
  @override
  _notificationState createState() => _notificationState();
}

class _notificationState extends State<notifications> {
  final storage = new FlutterSecureStorage();
  late FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  Future<List> _fetchHistory() async {
    try {
      String? token = await storage.read(key: constants.storage_key);
      print("token=" + token!);
      var url = Uri.parse(constants.API_URL + 'notifications/');
      var res = await http.get(url, headers: <String, String>{
        'Context-Type': 'application/json;charSet=UTF-8',
        'authorization': token
      });
      if (res.statusCode == 200) {
        print(jsonDecode(res.body)['data']);
        return jsonDecode(res.body)['data'];
      } else {
        return [];
      }
    } catch (e) {
      print(e);
      return [];
    }
  }

  Widget _listView(AsyncSnapshot<List> snapshot) {
    if (snapshot.connectionState == ConnectionState.waiting) {
      return Column(children: [
        Padding(
          padding: EdgeInsets.only(top: 16),
        ),
        SizedBox(
          child: CircularProgressIndicator(),
          width: 60,
          height: 60,
        ),
        Padding(
          padding: EdgeInsets.only(top: 16),
          child: Text('Loading...'),
        )
      ]);
    }

    print("length = " + snapshot.data!.length.toString());

    if (snapshot.data!.length > 0) {
      return ListView(
          children: snapshot.data!.map((var data) {
        print(data['file_url'] != null
            ? constants.PUBLIC_URL + data["file_url"]
            : '');
        return Container(
          height: 70,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 1,
                child: data['file_url'] != null
                    ? Container(
                        height: 70.0,
                        width: 70.0,
                        alignment: Alignment.center,
                        decoration: new BoxDecoration(
                          // color: constants.AppMainColor,
                          image: DecorationImage(
                              image: NetworkImage(
                                  constants.PUBLIC_URL + data["file_url"]),
                              fit: BoxFit.fitWidth,
                              alignment: Alignment.topCenter),
                          borderRadius: BorderRadius.circular(35.0),
                        ),
                        // child: Icon(
                        //   Icons.person_outline,
                        //   size: 30,
                        // ),
                      )
                    : Container(
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.grey.shade400),
                        child: Icon(
                          Icons.person,
                          color: Colors.white,
                        ),
                      ),
              ),
              Expanded(
                flex: 7,
                child: Container(
                  padding: EdgeInsets.fromLTRB(10, 5, 10, 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        child: Text(
                          data['message'],
                          overflow: TextOverflow.fade,
                          softWrap: false,
                          style: TextStyle(
                              fontSize: 18.0, fontWeight: FontWeight.w500),
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(width: 5),
                            Text(data['TimeStamp'].split("T")[0]),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      }).toList());
    } else {
      return Center(
        child: Text("No notifications to show Currently."),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: NewGradientAppBar(
            automaticallyImplyLeading: false,
            gradient: constants.appLinerGradient,
            title: Image.asset(
              'images/icon.png',
              height: 100,
              width: 100,
            )),
        body: Builder(
            builder: (context) => SafeArea(
                    child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(
                    left: 10,
                    right: 10,
                  ),
                  child: FutureBuilder(
                      future:
                          _fetchHistory(), // a previously-obtained Future<String> or null
                      builder:
                          (BuildContext context, AsyncSnapshot<List> snapshot) {
                        return _listView(snapshot);
                      }),
                ))));
  }
}
