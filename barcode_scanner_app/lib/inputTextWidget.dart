import 'package:FYD/model/user.dart';
import 'package:flutter/material.dart';
import 'constants.dart';

class InputTextWidget extends StatelessWidget {
  final String labelText;
  final IconData icon;
  final bool obscureText;
  final keyboardType;
  final controller;

  const InputTextWidget({
    required this.labelText,
    required this.icon,
    required this.obscureText,
    required this.keyboardType,
    this.controller,
  }) : super();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0.0, right: 0.0),
      child: Container(
        child: Material(
          shadowColor: Colors.black,
          borderRadius: BorderRadius.circular(15.0),
          child: Padding(
            padding: const EdgeInsets.only(right: 0.0, left: 0.0),
            child: TextFormField(
                controller: controller,
                obscureText: obscureText,
                autofocus: false,
                keyboardType: keyboardType,
                decoration: InputDecoration(
                  contentPadding: new EdgeInsets.symmetric(
                      vertical: 20.0, horizontal: 10.0),
                  prefixIcon: Icon(
                    icon,
                    color: constants.AppMainColor,
                    size: 25.0, /*Color(0xff224597)*/
                  ),
                  labelText: labelText,
                  labelStyle: TextStyle(color: Colors.black, fontSize: 18.0),
                  hintText: '',
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(
                        color: constants.AppMainColor, width: 1.5),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(
                      color: constants.AppMainColor,
                      width: 1.5,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(
                      color: Colors.grey,
                      width: 1.5,
                    ),
                  ),

                  // enabledBorder: InputBorder.none,
                  // focusedBorder: UnderlineInputBorder(
                  //   borderSide: BorderSide(color: Colors.transparent),
                  // ),
                  // border: InputBorder.none,
                ),
                validator: (val) {
                  if (val!.isEmpty) {
                    return 'Please enter a valid ' +
                        labelText.toLowerCase() +
                        '';
                  }
                }),
          ),
        ),
      ),
    );
  }
}
