import 'package:flutter/material.dart';
import 'constants.dart';
import 'package:dotted_decoration/dotted_decoration.dart';
import 'package:http/http.dart' as http;

import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:fluttertoast/fluttertoast.dart';

class history extends StatefulWidget {
  history({Key? key}) : super(key: key);
  @override
  _historyState createState() => _historyState();
}

class _historyState extends State<history> {
  final TextEditingController _pointsController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final storage = new FlutterSecureStorage();
  final TextEditingController _earnedPointsController = TextEditingController();
  final TextEditingController _minWithdrawController = TextEditingController();
  final TextEditingController _userNameController = TextEditingController();
  final TextEditingController _userContactController = TextEditingController();
  late FToast fToast;
  // ignore: non_constant_identifier_names
  String withdraw_point = '0';
  // ignore: non_constant_identifier_names
  String earned_point = '0';
  var configdata;
  var history;

  @override
  void initState() {
    super.initState();
    configdata = getConfigData();
    history = _fetchHistory();
    fToast = FToast();
    fToast.init(context);
  }

  Future<dynamic> getConfigData() async {
    String userId = await getUserId();
    var url = Uri.parse(constants.API_URL + 'configData/');
    String? token = await storage.read(key: constants.storage_key);
    // print("token = " + token!);
    var res = await http.get(url, headers: <String, String>{
      'Context-Type': 'application/json;charSet=UTF-8',
      'authorization': token.toString()
    });
    print(jsonDecode(res.body));
    if (res.statusCode == 200) {
      var jsonresponse = jsonDecode(res.body);
      var decodedToken = JwtDecoder.decode(token!);
      setState(() {
        withdraw_point = jsonDecode(res.body)['withdraw_limit'].toString();
      });
      jsonresponse.putIfAbsent("Name", () => decodedToken['Name']);
      jsonresponse.putIfAbsent(
          "ContactNumber", () => decodedToken['ContactNumber']);
      print('Config data is + ');
      print(jsonresponse);
      return Future.value(jsonresponse);
    } else {
      return -1;
    }
  }

  Future<String> getUserId() async {
    String? value = await storage.read(key: constants.storage_key);
    String valuekey = value.toString();
    // print(valuekey);
    var decodedToken = JwtDecoder.decode(valuekey);
    // print(decodedToken["_id"]);
    // print("decodedToken" + decodedToken.toString());
    _userNameController.text = decodedToken["Name"];
    _userContactController.text = decodedToken["ContactNumber"].toString();
    return decodedToken["_id"].toString();
  }

  Future<List> _fetchHistory() async {
    try {
      String userId = await getUserId();
      String? token = await storage.read(key: constants.storage_key);
      print("token=" + token!);
      var url = Uri.parse(constants.API_URL + 'gethistory/' + userId);
      var res = await http.get(url, headers: <String, String>{
        'Context-Type': 'application/json;charSet=UTF-8',
        'authorization': token
      });
      // print(jsonDecode(res.body));
      // print(res.statusCode);
      if (res.statusCode == 200) {
        setState(() {
          earned_point = jsonDecode(res.body)['scanned_points'].toString();
        });
        // _earnedPointsController.text =
        //     jsonDecode(res.body)['scanned_points'].toString();
        return jsonDecode(res.body)['data'];
      } else {
        return [];
      }
    } catch (e) {
      print(e);
      return [];
    }
  }

  _showToast(toasttext, toasticon, toastcolor) {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: toastcolor,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(toasticon),
          SizedBox(
            width: 12.0,
          ),
          Text(toasttext),
        ],
      ),
    );
  }

  redeemPoints(String points) async {
    String userId = await getUserId();
    String? token = await storage.read(key: constants.storage_key);
    print("token=" + token!);
    // if(earned_point)
    var url = Uri.parse(constants.API_URL + 'requestWithdraw/');
    var res = await http.post(url, headers: <String, String>{
      'Context-Type': 'application/json;charSet=UTF-8',
      'authorization': "Authorization: Bearer " + token
    }, body: <String, String>{
      'points': points,
      'user_id': userId
    });
    print(res.statusCode);
    if (res.statusCode == 200) {
      // _updatePoints(jsonDecode(res.body)['total_scanned'].toString());
      if (jsonDecode(res.body)['error'] != false) {
        constants.showToast(
            jsonDecode(res.body)['error'], Icons.close, Colors.red, fToast);
        print(jsonDecode(res.body)['error']);
      } else {
        constants.showToast(
            jsonDecode(res.body)['message'], Icons.check, Colors.green, fToast);
        getConfigData();
        print("successfully sent withdraw request");
      }
    } else {
      constants.showToast(
          "Something went wrong", Icons.close, Colors.red, fToast);
      return [];
    }
  }

  Widget _listView(AsyncSnapshot<List> snapshot) {
    if (snapshot.connectionState == ConnectionState.waiting) {
      return Column(children: [
        Padding(
          padding: EdgeInsets.only(top: 16),
        ),
        SizedBox(
          child: CircularProgressIndicator(),
          width: 60,
          height: 60,
        ),
        Padding(
          padding: EdgeInsets.only(top: 16),
          child: Text('Loading...'),
        )
      ]);
    }

    print("length = " + snapshot.data!.length.toString());

    if (snapshot.data!.length > 0) {
      return ListView(
          children: snapshot.data!.map((var data) {
        return Pointshistory(
          points: data["Points"].toString(),
          title:
              data["type"] == 'redeemed' ? "Points Redeemed" : "Points Earned",
          type: data["type"],
          date: data["date"].toString().split("T")[0],
        );
      }).toList());
    } else {
      return Center(
        child: Text("No History to show Currently."),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    print(withdraw_point);
    // getConfigData();
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Builder(
        builder: (context) => SafeArea(
          child: Container(
            child: SingleChildScrollView(
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                      height: 200,
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.all(15),
                      alignment: Alignment.centerLeft,
                      decoration: BoxDecoration(
                        gradient: constants.appLinerGradient,
                        borderRadius: BorderRadius.circular(10),
                        // color: Colors.amber,
                      ),
                      child: FutureBuilder(
                          future: configdata,
                          builder: (BuildContext context,
                              AsyncSnapshot<dynamic> snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return Center(
                                  child: SizedBox(
                                child: CircularProgressIndicator(),
                                width: 60,
                                height: 60,
                              ));
                            } else {
                              if (snapshot.hasError)
                                return Center(
                                    child: Text('Error: ${snapshot.error}'));
                              else {
                                return Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "My Points",
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.white),
                                    ),
                                    SizedBox(
                                      height: 7,
                                    ),
                                    Container(
                                      height: 100,
                                      // color: Colors.red,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            height: 90,
                                            width: 90,
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              border: Border.all(
                                                width: 4,
                                                color: Colors
                                                    .yellowAccent.shade700,
                                              ),
                                              color: Colors.transparent,
                                            ),
                                            child: Icon(
                                              Icons.emoji_events_outlined,
                                              color: Colors.white,
                                              size: 50,
                                            ),
                                          ),
                                          Expanded(
                                              child: ListTile(
                                            title: Text(earned_point + " pts",
                                                style: TextStyle(
                                                    fontSize: 30,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.yellowAccent
                                                        .shade700)),
                                            subtitle: Text(
                                                'Need ${snapshot.data!["withdraw_limit"]}' +
                                                    " points to redeem",
                                                style: TextStyle(
                                                    fontSize: 15,
                                                    color: Colors.white)),
                                          ))
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Container(
                                      height: 34,
                                      alignment: Alignment.center,
                                      decoration: DottedDecoration(
                                          linePosition: LinePosition.top,
                                          color: Colors.white),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Text('${snapshot.data!["Name"]}',
                                              style: TextStyle(
                                                  fontSize: 15,
                                                  color: Colors.white)),
                                          Text(
                                              '${snapshot.data!["ContactNumber"]}',
                                              style: TextStyle(
                                                  fontSize: 15,
                                                  color: Colors.white))
                                        ],
                                      ),
                                    )
                                  ],
                                );
                              }
                            }
                          })),
                  TabBar(
                    labelColor: Colors.black,
                    unselectedLabelColor: Colors.grey,
                    indicatorColor: constants.AppMainColor,
                    indicatorWeight: 4,
                    labelStyle:
                        TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    tabs: [
                      Tab(
                        text: "History",
                      ),
                      Tab(
                        text: "Redeem",
                      )
                    ],
                  ),
                  Container(
                      width: MediaQuery.of(context).size.width,
                      height: 325,
                      padding: EdgeInsets.only(top: 10),
                      child: TabBarView(
                        children: <Widget>[
                          // 1st Child i.e History
                          Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.only(
                              left: 10,
                              right: 10,
                            ),
                            child: FutureBuilder(
                                future:
                                    history, // a previously-obtained Future<String> or null
                                builder: (BuildContext context,
                                    AsyncSnapshot<List> snapshot) {
                                  return _listView(snapshot);
                                }),
                          ),
                          // 2nd Child i.e Redeem
                          Container(
                            width: MediaQuery.of(context).size.width,
                            // color: Colors.amber,
                            padding:
                                EdgeInsets.only(left: 10, right: 10, top: 15),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Redeem Points",
                                  style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.w700),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                ListTile(
                                  contentPadding: EdgeInsets.all(0),
                                  title: Text(
                                    "Current Balance",
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  trailing: Text(
                                    earned_point + " pts",
                                    style: TextStyle(
                                        fontSize: 20,
                                        color: constants.AppsecondaryColor,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Form(
                                    key: _formKey,
                                    child: Column(children: [
                                      Container(
                                        child: Material(
                                          shadowColor: Colors.black,
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                right: 0.0, left: 0.0),
                                            child: TextFormField(
                                                controller: _pointsController,
                                                obscureText: false,
                                                autofocus: false,
                                                keyboardType:
                                                    TextInputType.phone,
                                                decoration: InputDecoration(
                                                  contentPadding:
                                                      new EdgeInsets.symmetric(
                                                          vertical: 20.0,
                                                          horizontal: 10.0),
                                                  prefixIcon: Icon(
                                                    Icons.money,
                                                    color:
                                                        constants.AppMainColor,
                                                    size:
                                                        25.0, /*Color(0xff224597)*/
                                                  ),
                                                  labelText: "Points",
                                                  labelStyle: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 18.0),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15.0),
                                                    borderSide: BorderSide(
                                                        color: constants
                                                            .AppMainColor,
                                                        width: 1.5),
                                                  ),
                                                  border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15.0),
                                                    borderSide: BorderSide(
                                                      color: constants
                                                          .AppMainColor,
                                                      width: 1.5,
                                                    ),
                                                  ),
                                                  enabledBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15.0),
                                                    borderSide: BorderSide(
                                                      color: Colors.grey,
                                                      width: 1.5,
                                                    ),
                                                  ),
                                                ),
                                                validator: (val) {
                                                  if (val!.isEmpty ||
                                                      int.parse(val) <
                                                          int.parse(
                                                              withdraw_point) ||
                                                      (int.parse(earned_point) <
                                                          int.parse(val))) {
                                                    return 'Please enter a valid number of points';
                                                  }
                                                }),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 12.0,
                                      ),
                                      Text(
                                        "You need min " +
                                            withdraw_point +
                                            " points to redeem",
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            fontSize: 15,
                                            color: Colors.grey.shade700),
                                      ),
                                      Container(
                                        margin: const EdgeInsets.symmetric(
                                            horizontal: 0, vertical: 10),
                                        child: RaisedButton(
                                          onPressed: () {
                                            if (_formKey.currentState!
                                                .validate()) {
                                              var withdra_points = int.parse(
                                                  _pointsController.text);
                                              var earned_points =
                                                  int.parse(earned_point);

                                              if (withdra_points <=
                                                  earned_points) {
                                                redeemPoints(
                                                    _pointsController.text);
                                                _pointsController.clear();
                                                print(
                                                    "Points Entered Correctly.");
                                              } else {
                                                _showToast(
                                                    "Please Enter Valid Point",
                                                    Icons.close,
                                                    Colors.red);
                                              }
                                            }
                                          },
                                          color: constants.AppMainColor,
                                          shape: const RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(14))),
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 8, horizontal: 8),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: <Widget>[
                                                Text(
                                                  'Redeem',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontSize: 18),
                                                ),
                                                Container(
                                                  padding:
                                                      const EdgeInsets.all(8),
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        const BorderRadius.all(
                                                            Radius.circular(
                                                                20)),
                                                    color: constants
                                                        .AppsecondaryColor,
                                                  ),
                                                  child: Icon(
                                                    Icons.arrow_forward_ios,
                                                    color: Colors.white,
                                                    size: 16,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      )
                                    ]))
                              ],
                            ),
                          ),
                        ],
                      ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class Pointshistory extends StatelessWidget {
  final String title;
  final String points;
  final String date;
  final String type;

  Pointshistory(
      {Key? key,
      required this.points,
      required this.title,
      required this.date,
      required this.type})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 1,
            child: Container(
              alignment: Alignment.center,
              child: Icon(
                Icons.account_balance_wallet_outlined,
                color: constants.AppMainColor,
              ),
            ),
          ),
          Expanded(
            flex: 8,
            child: Container(
                padding: const EdgeInsets.fromLTRB(8, 10, 8, 10),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(color: Colors.grey, width: 1))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                        // flex: 8,
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          width: double.infinity,
                          child: Text(
                            title,
                            overflow: TextOverflow.fade,
                            softWrap: false,
                            style: TextStyle(
                                fontSize: 17.0, fontWeight: FontWeight.w500),
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          child: Text(date),
                        )
                      ],
                    )),
                    Text(
                      type == 'earned' ? "+" + points : "-" + points,
                      style: TextStyle(
                          color: type == 'earned' ? Colors.green : Colors.red,
                          fontSize: 18,
                          fontWeight: FontWeight.w600),
                    ),
                  ],
                )),
          ),
        ],
      ),
    );
  }
}
