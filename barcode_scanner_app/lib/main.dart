import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'userLogin.dart';
import 'userLoginMobile.dart';
import 'userLoginOtp.dart';
import 'responsive.dart';
import 'userHome.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  Future<String> autologin() async {
    var sharedInstance = await SharedPreferences.getInstance();

    if (sharedInstance.getString('token') != null) {
      // Map userdata = sharedInstance.getString('Userdata') as Map;
      // print(sharedInstance.getString('token'));
      // print('tocken_data----------->');
      // print(sharedInstance.getString('token'));
      String tokenkey = sharedInstance.getString('token').toString();
      return tokenkey;
    } else {
      // print('Dont find token !!!!!');
      return '';
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Barcode Scanner',
      debugShowCheckedModeBanner: false,
      // initialRoute: '/loginwithotp',
      routes: {
        '/loginpageemail': (context) => userlogin(),
        '/loginpagemobile': (context) => userloginmobile(),
        '/loginwithotp': (context) => userLoginOtp()
      },
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // home: Builder(builder: (context) {
      //   ResponsiveApp.setMq(context);
      //   return userloginmobile();
      // }),
      home: FutureBuilder(
        future: autologin(),
        builder: (context, autoresult) {
          if (autoresult.connectionState == ConnectionState.waiting) {
            return CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
            );
          } else {
            if (autoresult.data != "") {
              // print('autoresult.data-------------->');
              // print(autoresult.data);
              var tokenkey = autoresult.data.toString();
              return userHome(
                tokenkey: tokenkey,
              );
            }
            return userLoginOtp();
          }
        },
      ),
    );
  }
}
