import 'dart:convert';
// import 'dart:ffi';

import 'package:FYD/editBankDetails.dart';
import 'package:FYD/userHome.dart';
import 'package:FYD/notifications.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'constants.dart';
import 'bankDetails.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:http/http.dart' as http;
import 'package:FYD/model/bankdetails.dart';
import 'package:fluttertoast/fluttertoast.dart';

class userProfile extends StatefulWidget {
  userProfile({Key? key}) : super(key: key);
  @override
  _userProfileState createState() => _userProfileState();
}

class _userProfileState extends State<userProfile> {
  final storage = new FlutterSecureStorage();
  String bankdatapresent = "bankdatapresent";
  late FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceWebView: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<Map<String, dynamic>> getData() async {
    String? value = await storage.read(key: constants.storage_key);
    String valuekey = value!;
    // print(valuekey);
    Map<String, dynamic> decodedToken = JwtDecoder.decode(valuekey);
    print(decodedToken["_id"]);
    var url =
        Uri.parse(constants.API_URL + 'bank-details/${decodedToken["_id"]}');
    // print(url);
    var res = await http.get(url, headers: <String, String>{
      'Context-Type': 'application/json;charSet=UTF-8',
      'authorization': valuekey
    });
    // print(res.body);
    Map<String, dynamic> jsonresponse = json.decode(res.body);
    var bankdata = jsonresponse['data'];
    jsonresponse['data'] != null
        ? decodedToken.putIfAbsent(bankdatapresent, () => true)
        : decodedToken.putIfAbsent(bankdatapresent, () => false);
    if (jsonresponse['data'] != null) {
      decodedToken.putIfAbsent(bankdatapresent, () => true);
      decodedToken.putIfAbsent("AccountNo", () => bankdata['AccountNo']);
      decodedToken.putIfAbsent("Confirmaccno", () => bankdata['Confirmaccno']);
      decodedToken.putIfAbsent("BankName", () => bankdata['BankName']);
      decodedToken.putIfAbsent("IFSC", () => bankdata['IFSC']);
      decodedToken.putIfAbsent("Branch", () => bankdata['Branch']);
    }
    // print(decodedToken);
    // print(res.statusCode);
    return Future.value(decodedToken);
  }

  Future deleteBankDetails() async {
    String? value = await storage.read(key: constants.storage_key);
    String valuekey = value!;
    print("delete user bank details token = " + valuekey);
    Map<String, dynamic> decodedToken = JwtDecoder.decode(valuekey);
    print(decodedToken["_id"]);
    var url =
        Uri.parse(constants.API_URL + 'bank-details/${decodedToken["_id"]}');
    print(url);
    var res = await http.delete(url, headers: <String, String>{
      'Context-Type': 'application/json;charSet=UTF-8',
      'authorization': valuekey
    });
    print(jsonDecode(res.body));
    if (res.statusCode == 200) {
      constants.showToast("Bank Details Deleted Successfully.", Icons.check,
          Colors.green, fToast);
      Navigator.push(
          context,
          new MaterialPageRoute(
              builder: (context) => userHome(
                    tokenkey: valuekey,
                  )));
    } else {
      constants.showToast("Failed to Delete Bank Details. Try Again Later",
          Icons.close, Colors.red, fToast);
    }
  }

  Future<String> getConfigData() async {
    var url = Uri.parse(constants.API_URL + 'configData/');
    var token = await storage.read(key: 'lokintoken');
    var res = await http.get(url, headers: <String, String>{
      'Context-Type': 'application/json;charSet=UTF-8',
      'authorization': token.toString()
    });
    if (res.statusCode == 200) {
      Map<String, dynamic> jsonresponse = json.decode(res.body);
      print('Config data is + ');
      print(jsonresponse);
      return jsonresponse['product_catlog'].toString();
    } else {
      return '';
    }
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final double r = (175 / 360);
    final coverHeight = screenWidth * r;
    return FutureBuilder<Map<String, dynamic>>(
      future: getData(),
      builder:
          (BuildContext context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
              child: LoadingIndicator(
                  indicatorType: Indicator.ballClipRotatePulse,
                  colors: const [Colors.white],
                  strokeWidth: 2,
                  backgroundColor: Colors.white,
                  pathBackgroundColor: Colors.white));
        } else {
          if (snapshot.hasError)
            return Center(child: Text('Error: ${snapshot.error}'));
          else
            return Center(
                child: Scaffold(
              body: Builder(
                builder: (context) => SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 70,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Icon(
                                      Icons.apartment_outlined,
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 8,
                                  child: Container(
                                    padding:
                                        const EdgeInsets.fromLTRB(8, 10, 8, 10),
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom: BorderSide(
                                                color: Colors.grey, width: 1))),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Container(
                                          width: double.infinity,
                                          child: Text(
                                            "Firm Name",
                                            overflow: TextOverflow.fade,
                                            softWrap: false,
                                            style: TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Container(
                                          width: double.infinity,
                                          child: Text(
                                              '${snapshot.data!["Firmname"]}'),
                                        )
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            height: 70,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Icon(
                                      Icons.person,
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 8,
                                  child: Container(
                                    padding:
                                        const EdgeInsets.fromLTRB(8, 10, 8, 10),
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom: BorderSide(
                                                color: Colors.grey, width: 1))),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Container(
                                          width: double.infinity,
                                          child: Text(
                                            "Full Name",
                                            overflow: TextOverflow.fade,
                                            softWrap: false,
                                            style: TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Container(
                                          width: double.infinity,
                                          child:
                                              Text('${snapshot.data!["Name"]}'),
                                        )
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            height: 70,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Icon(Icons.call),
                                  ),
                                ),
                                Expanded(
                                  flex: 8,
                                  child: Container(
                                    padding:
                                        const EdgeInsets.fromLTRB(8, 10, 8, 10),
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom: BorderSide(
                                                color: Colors.grey, width: 1))),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Container(
                                          width: double.infinity,
                                          child: Text(
                                            "Phone Number",
                                            overflow: TextOverflow.fade,
                                            softWrap: false,
                                            style: TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Container(
                                          width: double.infinity,
                                          child: Text(
                                              '${snapshot.data!["ContactNumber"]}'),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            height: 70,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Icon(Icons.email_outlined),
                                  ),
                                ),
                                Expanded(
                                  flex: 8,
                                  child: Container(
                                    padding:
                                        const EdgeInsets.fromLTRB(8, 10, 8, 10),
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom: BorderSide(
                                                color: Colors.grey, width: 1))),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Container(
                                          width: double.infinity,
                                          child: Text(
                                            "Email Id",
                                            overflow: TextOverflow.fade,
                                            softWrap: false,
                                            style: TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Container(
                                          width: double.infinity,
                                          child: Text(
                                              '${snapshot.data!["Email"]}'),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            height: 70,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Icon(Icons.home),
                                  ),
                                ),
                                Expanded(
                                  flex: 8,
                                  child: Container(
                                    padding:
                                        const EdgeInsets.fromLTRB(8, 10, 8, 10),
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom: BorderSide(
                                                color: Colors.grey, width: 1))),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Container(
                                          width: double.infinity,
                                          child: Text(
                                            "Address of the Firm",
                                            overflow: TextOverflow.fade,
                                            softWrap: false,
                                            style: TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Container(
                                          width: double.infinity,
                                          child: Text(
                                              '${snapshot.data!["Address"]}'),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          snapshot.data!["Aadhar"] != ''
                              ? Container(
                                  height: 70,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                          alignment: Alignment.center,
                                          child: Icon(Icons.pin),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 8,
                                        child: Container(
                                          padding: const EdgeInsets.fromLTRB(
                                              8, 10, 8, 10),
                                          decoration: snapshot.data!["GSTNo"] !=
                                                  ''
                                              ? BoxDecoration(
                                                  border: Border(
                                                      bottom: BorderSide(
                                                          color: Colors.grey,
                                                          width: 1)))
                                              : BoxDecoration(),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Container(
                                                width: double.infinity,
                                                child: Text(
                                                  "Aadhaar Number",
                                                  overflow: TextOverflow.fade,
                                                  softWrap: false,
                                                  style: TextStyle(
                                                      fontSize: 15.0,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                              ),
                                              Container(
                                                width: double.infinity,
                                                child: Text(
                                                    '${snapshot.data!["Aadhar"]}'),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              : SizedBox(
                                  height: 1,
                                ),
                          snapshot.data!["GSTNo"] != ''
                              ? Container(
                                  height: 70,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                          alignment: Alignment.center,
                                          child: Icon(Icons.pin),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 8,
                                        child: Container(
                                          padding: const EdgeInsets.fromLTRB(
                                              8, 10, 8, 10),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Container(
                                                width: double.infinity,
                                                child: Text(
                                                  "GST Number",
                                                  overflow: TextOverflow.fade,
                                                  softWrap: false,
                                                  style: TextStyle(
                                                      fontSize: 15.0,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                              ),
                                              Container(
                                                width: double.infinity,
                                                child: Text(
                                                    '${snapshot.data!["GSTNo"]}'),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              : SizedBox(
                                  height: 1,
                                ),

                          SizedBox(
                            height: 20,
                          ),
                          // snapshot.data!["bankdatapresent"]
                          //     ? Container(
                          //         padding: const EdgeInsets.symmetric(
                          //             vertical: 10, horizontal: 15),
                          //         decoration: BoxDecoration(
                          //             color: constants.AppMainColor,
                          //             borderRadius: BorderRadius.all(
                          //                 Radius.circular(14))),
                          //         child: Row(
                          //           mainAxisAlignment:
                          //               MainAxisAlignment.start,
                          //           children: <Widget>[
                          //             Expanded(
                          //               flex: 7,
                          //               child: Text(
                          //                 'Bank Details',
                          //                 textAlign: TextAlign.left,
                          //                 style: TextStyle(
                          //                     color: Colors.white,
                          //                     fontWeight: FontWeight.w600,
                          //                     fontSize: 18),
                          //               ),
                          //             ),
                          //             // Expanded(
                          //             //   flex: 2,
                          //             //   child: CircleAvatar(
                          //             //     radius: 20,
                          //             //     backgroundColor:
                          //             //         constants.AppsecondaryColor,
                          //             //     child: IconButton(
                          //             //       onPressed: () {
                          //             //         print("edit");
                          //             //         Bankdetails allbankdata =
                          //             //             new Bankdetails(
                          //             //                 snapshot.data!["_id"],
                          //             //                 snapshot
                          //             //                     .data!["AccountNo"],
                          //             //                 snapshot.data![
                          //             //                     "Confirmaccno"],
                          //             //                 snapshot
                          //             //                     .data!["BankName"],
                          //             //                 snapshot
                          //             //                     .data!["Branch"],
                          //             //                 snapshot.data!["IFSC"]);
                          //             //         Navigator.pushReplacement(
                          //             //           context,
                          //             //           MaterialPageRoute(
                          //             //               builder: (context) =>
                          //             //                   editbankDetails(
                          //             //                     iseditable: false,
                          //             //                     allbankdetails:
                          //             //                         allbankdata,
                          //             //                   )),
                          //             //         );
                          //             //       },
                          //             //       icon: Icon(Icons.edit),
                          //             //       iconSize: 16,
                          //             //       color: Colors.white,
                          //             //     ),
                          //             //   ),
                          //             // ),
                          //             Expanded(
                          //               flex: 2,
                          //               child: CircleAvatar(
                          //                 radius: 20,
                          //                 backgroundColor:
                          //                     constants.AppsecondaryColor,
                          //                 child: IconButton(
                          //                   onPressed: () {
                          //                     print("view");
                          //                     Bankdetails allbankdata =
                          //                         new Bankdetails(
                          //                             snapshot.data!["_id"],
                          //                             snapshot
                          //                                 .data!["AccountNo"],
                          //                             snapshot.data![
                          //                                 "Confirmaccno"],
                          //                             snapshot
                          //                                 .data!["BankName"],
                          //                             snapshot
                          //                                 .data!["Branch"],
                          //                             snapshot.data!["IFSC"]);
                          //                     Navigator.pushReplacement(
                          //                       context,
                          //                       MaterialPageRoute(
                          //                           builder: (context) =>
                          //                               editbankDetails(
                          //                                 iseditable: true,
                          //                                 allbankdetails:
                          //                                     allbankdata,
                          //                               )),
                          //                     );
                          //                   },
                          //                   icon: Icon(Icons.visibility),
                          //                   iconSize: 16,
                          //                   color: Colors.white,
                          //                 ),
                          //               ),
                          //             ),
                          //             Expanded(
                          //               flex: 2,
                          //               child: CircleAvatar(
                          //                 radius: 20,
                          //                 backgroundColor:
                          //                     constants.AppsecondaryColor,
                          //                 child: IconButton(
                          //                   onPressed: () {
                          //                     print("delete");
                          //                     showBottomSheet(
                          //                         backgroundColor:
                          //                             Colors.white,
                          //                         context: context,
                          //                         builder:
                          //                             (context) => Padding(
                          //                                   padding:
                          //                                       const EdgeInsets
                          //                                           .all(8.0),
                          //                                   child: Container(
                          //                                     width: double
                          //                                         .infinity,
                          //                                     height: 120.0,
                          //                                     decoration: BoxDecoration(
                          //                                         color: Colors
                          //                                             .white,
                          //                                         borderRadius: BorderRadius.all(Radius.circular(15)),
                          //                                         boxShadow: [
                          //                                           BoxShadow(
                          //                                               blurRadius:
                          //                                                   10,
                          //                                               color: Colors
                          //                                                   .grey
                          //                                                   .shade300,
                          //                                               spreadRadius:
                          //                                                   5)
                          //                                         ]),
                          //                                     child: Column(
                          //                                       children: <
                          //                                           Widget>[
                          //                                         Padding(
                          //                                           padding: const EdgeInsets
                          //                                                   .all(
                          //                                               15.0),
                          //                                           child:
                          //                                               Text(
                          //                                             'Delete Bank Details? ',
                          //                                             style: TextStyle(
                          //                                                 fontSize:
                          //                                                     20,
                          //                                                 fontWeight:
                          //                                                     FontWeight.w800),
                          //                                           ),
                          //                                         ),
                          //                                         Padding(
                          //                                           padding:
                          //                                               const EdgeInsets.fromLTRB(
                          //                                                   20,
                          //                                                   5,
                          //                                                   20,
                          //                                                   0),
                          //                                           child:
                          //                                               Row(
                          //                                             mainAxisAlignment:
                          //                                                 MainAxisAlignment.spaceBetween,
                          //                                             children: <
                          //                                                 Widget>[
                          //                                               TextButton(
                          //                                                 onPressed:
                          //                                                     () {
                          //                                                   Navigator.of(context).pop();
                          //                                                 },
                          //                                                 child:
                          //                                                     Padding(
                          //                                                   padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                          //                                                   child: Text(
                          //                                                     'Cancel',
                          //                                                     style: TextStyle(color: Colors.white, fontSize: 16.0),
                          //                                                   ),
                          //                                                 ),
                          //                                                 style:
                          //                                                     ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(constants.AppMainColor), shape: MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)))),
                          //                                               ),
                          //                                               TextButton(
                          //                                                 onPressed:
                          //                                                     () {
                          //                                                   deleteBankDetails();
                          //                                                 },
                          //                                                 child:
                          //                                                     Padding(
                          //                                                   padding: const EdgeInsets.fromLTRB(30, 8, 30, 8),
                          //                                                   child: Text(
                          //                                                     'Yes',
                          //                                                     style: TextStyle(color: Colors.white, fontSize: 16.0),
                          //                                                   ),
                          //                                                 ),
                          //                                                 style:
                          //                                                     ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(constants.AppMainColor), shape: MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)))),
                          //                                               ),
                          //                                             ],
                          //                                           ),
                          //                                         )
                          //                                       ],
                          //                                     ),
                          //                                   ),
                          //                                 ));
                          //                     // deleteBankDetails();
                          //                     // Navigator.pushReplacement(
                          //                     //   context,
                          //                     //   MaterialPageRoute(
                          //                     //       builder: (context) =>
                          //                     //           userHome(
                          //                     //               tokenkey: constants
                          //                     //                   .storage_key)),
                          //                     // );
                          //                   },
                          //                   icon: Icon(Icons.delete),
                          //                   iconSize: 16,
                          //                   color: Colors.white,
                          //                 ),
                          //               ),
                          //             ),
                          //           ],
                          //         ),
                          //       )
                          //     : RaisedButton(
                          //         onPressed: () {
                          //           Navigator.pushReplacement(
                          //             context,
                          //             MaterialPageRoute(
                          //               builder: (context) => bankDetails(),
                          //             ),
                          //           );
                          //           // Navigator.push(
                          //           //     context,
                          //           //     new MaterialPageRoute(
                          //           //         builder: (context) =>
                          //           //             bankDetails()));
                          //         },
                          //         color: constants.AppMainColor,
                          //         shape: const RoundedRectangleBorder(
                          //             borderRadius: BorderRadius.all(
                          //                 Radius.circular(14))),
                          //         child: Container(
                          //           padding: const EdgeInsets.symmetric(
                          //               vertical: 8, horizontal: 8),
                          //           child: Row(
                          //             mainAxisAlignment:
                          //                 MainAxisAlignment.spaceBetween,
                          //             children: <Widget>[
                          //               Text(
                          //                 'Add Bank Details',
                          //                 style: TextStyle(
                          //                     color: Colors.white,
                          //                     fontWeight: FontWeight.w600,
                          //                     fontSize: 18),
                          //               ),
                          //               Container(
                          //                 padding: const EdgeInsets.all(8),
                          //                 decoration: BoxDecoration(
                          //                   borderRadius:
                          //                       const BorderRadius.all(
                          //                           Radius.circular(20)),
                          //                   color:
                          //                       constants.AppsecondaryColor,
                          //                 ),
                          //                 child: Icon(
                          //                   Icons.arrow_forward_ios,
                          //                   color: Colors.white,
                          //                   size: 16,
                          //                 ),
                          //               )
                          //             ],
                          //           ),
                          //         ),
                          //       ),
                          // SizedBox(
                          //   height: 20,
                          // ),
                          Container(
                            height: 60.0,
                            child: ElevatedButton(
                              onPressed: () async {
                                // ignore: unrelated_type_equality_checks
                                var productCatlogUrl = await getConfigData();
                                String url = productCatlogUrl.toString();
                                print(url.toString());
                                if (url != '') {
                                  launchURL(url);
                                } else {
                                  constants.showToast('Url not found',
                                      Icons.close, Colors.red, fToast);
                                }
                              },
                              style: ElevatedButton.styleFrom(
                                primary: Colors.white,
                                elevation: 0.0,
                                minimumSize: Size(screenWidth, 150),
                                padding: EdgeInsets.symmetric(horizontal: 0),
                                shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(0)),
                                ),
                              ),
                              child: Ink(
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                        begin: Alignment.centerLeft,
                                        end: Alignment.centerRight,
                                        colors: [
                                          Colors.blueAccent,
                                          Colors.blueAccent
                                        ]),
                                    borderRadius: BorderRadius.circular(10.0)),
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    "Product Listing",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 20),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 60.0,
                            child: ElevatedButton(
                              onPressed: () async {
                                Navigator.push(
                                    context,
                                    new MaterialPageRoute(
                                        builder: (context) => notifications()));
                              },
                              style: ElevatedButton.styleFrom(
                                primary: Colors.white,
                                elevation: 0.0,
                                minimumSize: Size(screenWidth, 150),
                                padding: EdgeInsets.symmetric(horizontal: 0),
                                shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(0)),
                                ),
                              ),
                              child: Ink(
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                        begin: Alignment.centerLeft,
                                        end: Alignment.centerRight,
                                        colors: [
                                          Colors.blueAccent,
                                          Colors.blueAccent
                                        ]),
                                    borderRadius: BorderRadius.circular(10.0)),
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    "Notifications",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 20),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            )); // snapshot.data  :- get your object which is pass from your downloadData() function
        }
      },
    );
  }
}
