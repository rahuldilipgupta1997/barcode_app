import 'package:FYD/userLoginMobile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'constants.dart';
import 'scanner.dart';
import 'history.dart';
import 'userProfile.dart';
import 'userLogin.dart';
import 'coupons.dart';
import 'package:new_gradient_app_bar/new_gradient_app_bar.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class userHome extends StatefulWidget {
  final String tokenkey;
  userHome({Key? key, required this.tokenkey}) : super(key: key);
  @override
  _userHomeState createState() => _userHomeState();
}

class _userHomeState extends State<userHome> {
  int index = 0;
  // String key;
  final storage = new FlutterSecureStorage();

  static List<Widget> _widgetOption = <Widget>[
    history(),
    coupons(),
    scanner(),
    userProfile(),
  ];

  @override
  Widget build(BuildContext context) {
    String tokenkey = widget.tokenkey;

    Future<bool> showExitPopup() async {
      return await showDialog(
            //show confirm dialogue
            //the return value will be from "Yes" or "No" options
            context: context,
            builder: (context) => AlertDialog(
              title: Text('Exit App'),
              content: Text('Do you want to exit the App?'),
              actions: [
                ElevatedButton(
                  onPressed: () => Navigator.of(context).pop(false),
                  //return false when click on "NO"
                  child: Text('No'),
                  style: ElevatedButton.styleFrom(primary: Colors.red),
                ),
                ElevatedButton(
                  onPressed: () async {
                    await storage.delete(key: tokenkey);
                    String? value = await storage.read(key: tokenkey);
                    print(value);
                    SystemNavigator.pop();
                  },
                  //return true when click on "Yes"
                  child: Text('Yes'),
                ),
              ],
            ),
          ) ??
          false; //if showDialouge had returned null, then return false
    }

    return WillPopScope(
      onWillPop: showExitPopup,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: NewGradientAppBar(
            automaticallyImplyLeading: false,
            gradient: constants.appLinerGradient,
            title: Image.asset(
              'images/icon.png',
              height: 100,
              width: 100,
            ),
            actions: [
              Padding(
                padding: EdgeInsets.only(right: 10),
                child: PopupMenuButton(
                  offset: Offset(0, 50),
                  child: Center(
                    child: Icon(Icons.more_vert_outlined),
                  ),
                  itemBuilder: (context) => [
                    PopupMenuItem(
                        child: TextButton(
                      child: Text(
                        'Logout',
                        style: TextStyle(color: constants.AppMainColor),
                      ),
                      onPressed: () async {
                        print("Logout Clicked");
                        await storage.delete(key: tokenkey);
                        String? value = await storage.read(key: tokenkey);
                        print(value);
                        print(value);
                        print("Delete the sharepreference data---------->");
                        var preferences = await SharedPreferences.getInstance();
                        await preferences.clear();
                        Navigator.push(
                            context,
                            new MaterialPageRoute(
                                builder: (context) => userloginmobile()));
                      },
                    )),
                  ],
                ),
              )
            ]),
        body: _widgetOption[index],
        bottomNavigationBar: new Theme(
          data: Theme.of(context).copyWith(
              canvasColor: Colors.white,
              primaryColor: constants.AppMainColor,
              textTheme: Theme.of(context)
                  .textTheme
                  .copyWith(caption: new TextStyle(color: Colors.grey))),
          child: BottomNavigationBar(
            selectedItemColor: constants.AppMainColor,
            type: BottomNavigationBarType.fixed,
            currentIndex: index,
            onTap: (int index) {
              print(index);
              setState(() {
                this.index = index;
              });
            },
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.account_balance_wallet_outlined),
                label: 'Wallet',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.sell_outlined),
                label: 'Coupons',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.qr_code_scanner_outlined),
                label: 'Scan',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person),
                label: 'Profile',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
