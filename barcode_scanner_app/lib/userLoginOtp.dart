import 'package:flutter/material.dart';
import 'constants.dart';
import 'inputTextWidget.dart';
import 'otpPage.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:fluttertoast/fluttertoast.dart';
import 'userSignup.dart';
import 'userLoginMobile.dart';
import 'userLogin.dart';

class userLoginOtp extends StatefulWidget {
  userLoginOtp({Key? key}) : super(key: key);
  @override
  _userLoginOtpState createState() => _userLoginOtpState();
}

class _userLoginOtpState extends State<userLoginOtp> {
  final TextEditingController _phoneController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  late FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final double r = (175 / 360);
    final coverHeight = screenWidth * r;
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      // appBar: AppBar(
      //   elevation: 0,
      //   backgroundColor: Colors.white,
      //   iconTheme: IconThemeData(
      //     color: Colors.black, //change your color here
      //   ),
      // ),
      body: Builder(
        builder: (context) => SafeArea(
          child: Container(
            // height: 50,
            // height: MediaQuery.of(context).size.height,
            padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
            width: double.infinity,
            alignment: Alignment.center,
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Container(
                      height: 130,
                      width: 130,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: AssetImage('images/icon.png'))),
                      alignment: Alignment.center,
                    ),
                  ),
                  Container(
                    height: 50,
                    width: double.infinity,
                    alignment: Alignment.center,
                    child: Text(
                      "Sign In to continue",
                      style: TextStyle(
                          fontSize: 27,
                          color: Colors.black,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                      child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(children: <TextSpan>[
                      TextSpan(
                          text: 'We will send you an ',
                          style: TextStyle(color: constants.AppsecondaryColor)),
                      TextSpan(
                          text: 'One Time Password ',
                          style: TextStyle(
                              color: constants.AppMainColor,
                              fontWeight: FontWeight.bold)),
                      TextSpan(
                          text: 'on this number',
                          style: TextStyle(color: constants.AppsecondaryColor)),
                    ]),
                  )),
                  SizedBox(
                    height: 20,
                  ),
                  Form(
                    key: _formKey,
                    child: Column(children: [
                      Container(
                        child: Material(
                          shadowColor: Colors.black,
                          borderRadius: BorderRadius.circular(15.0),
                          child: Padding(
                            padding:
                                const EdgeInsets.only(right: 0.0, left: 0.0),
                            child: TextFormField(
                                controller: _phoneController,
                                obscureText: false,
                                autofocus: false,
                                keyboardType: TextInputType.phone,
                                decoration: InputDecoration(
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 20.0, horizontal: 10.0),
                                  prefixIcon: Icon(
                                    Icons.pin,
                                    color: constants.AppMainColor,
                                    size: 25.0, /*Color(0xff224597)*/
                                  ),
                                  labelText: "Mobile Number",
                                  labelStyle: TextStyle(
                                      color: Colors.black, fontSize: 18.0),
                                  hintText: '+91...',
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                    borderSide: BorderSide(
                                        color: constants.AppMainColor,
                                        width: 1.5),
                                  ),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                    borderSide: BorderSide(
                                      color: constants.AppMainColor,
                                      width: 1.5,
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                    borderSide: BorderSide(
                                      color: Colors.grey,
                                      width: 1.5,
                                    ),
                                  ),
                                ),
                                validator: (val) {
                                  if (val!.isEmpty || val.length != 10) {
                                    return 'Please enter a valid 10 digit Mobile Number';
                                  }
                                }),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 12.0,
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(
                            horizontal: 0, vertical: 10),
                        child: RaisedButton(
                          onPressed: () async {
                            if (_formKey.currentState!.validate()) {
                              constants.showToast('sendig otp', Icons.pending,
                                  Colors.green.shade200, fToast);
                              var url =
                                  Uri.parse(constants.API_URL + 'send_otp');
                              var res = await http
                                  .post(url, headers: <String, String>{
                                'Context-Type': 'application/json;charSet=UTF-8'
                              }, body: <String, String>{
                                'phone': _phoneController.text,
                              });
                              print(res.body);
                              print(res.statusCode);
                              if (res.statusCode == 200) {
                                print("Mobile Number Entered Correctly.");
                                Map<String, dynamic> jsonresponse =
                                    json.decode(res.body);
                                if (jsonresponse['error'] != false) {
                                  //error
                                  constants.showToast(jsonresponse['error'],
                                      Icons.close, Colors.red, fToast);
                                } else {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => OtpPage(
                                              phone_no: _phoneController.text,
                                            )),
                                  );
                                }
                              } else {
                                constants.showToast("Something went wrong",
                                    Icons.close, Colors.red, fToast);
                                //error
                              }
                            } else {
                              constants.showToast("Something went wrong",
                                  Icons.close, Colors.red, fToast);
                              //error
                            }
                          },
                          color: constants.AppMainColor,
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(14))),
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 8),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  'Next',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18),
                                ),
                                Container(
                                  padding: const EdgeInsets.all(8),
                                  decoration: BoxDecoration(
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(20)),
                                    color: constants.AppsecondaryColor,
                                  ),
                                  child: Icon(
                                    Icons.arrow_forward_ios,
                                    color: Colors.white,
                                    size: 16,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ]),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 50,
                    // color: Colors.amber,
                    padding: EdgeInsets.only(left: 10, right: 10),
                    width: double.infinity,
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 4,
                          child: Container(
                            height: 1,
                            color: Colors.grey,
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                            alignment: Alignment.center,
                            child: Text(
                              "OR",
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.grey[700]),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Container(
                            height: 1,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 40,
                    // color: Colors.amberAccent,
                    alignment: Alignment.center,
                    child: TextButton(
                      child: Text(
                        "Log in with Mobile Number",
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            color: constants.AppMainColor),
                      ),
                      onPressed: () {
                        print("Mobile Number Login pressed");
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => userloginmobile()),
                        );
                      },
                    ),
                  ),
                  Container(
                    height: 40,
                    // color: Colors.amberAccent,
                    alignment: Alignment.center,
                    child: TextButton(
                      child: Text(
                        "Log in with Email Id",
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            color: constants.AppMainColor),
                      ),
                      onPressed: () {
                        print("Email Id Login pressed");
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => userlogin()),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        height: 50.0,
        color: Colors.white,
        child: Center(
            child: Wrap(
          children: [
            Text(
              "Don't have an account?  ",
              style: TextStyle(
                  color: Colors.grey[600],
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
            ),
            Material(
                child: InkWell(
              onTap: () {
                print("sign up tapped");
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => userSignup()),
                );
              },
              child: Text(
                "Sign Up",
                style: TextStyle(
                  color: constants.AppMainColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
            )),
          ],
        )),
      ),
    );
  }
}
