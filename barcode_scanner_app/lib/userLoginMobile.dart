import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'constants.dart';
import 'inputTextWidget.dart';
import 'userSignup.dart';
import 'userHome.dart';
import 'userLogin.dart';
import 'userLoginOtp.dart';
import 'package:FYD/model/user.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'constants.dart';
import 'package:fluttertoast/fluttertoast.dart';

class userloginmobile extends StatefulWidget {
  const userloginmobile({Key? key}) : super(key: key);
  @override
  _userloginmobileState createState() => _userloginmobileState();
}

class _userloginmobileState extends State<userloginmobile> {
  final TextEditingController _pwdController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final storage = new FlutterSecureStorage();
  late FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  Future save() async {
    var url = Uri.parse(constants.API_URL + 'login');
    var res = await http.post(url, headers: <String, String>{
      'Context-Type': 'application/json;charSet=UTF-8'
    }, body: <String, String>{
      'ContactNumber': user.mobnumber,
      'Password': user.password
    });
    print(res.body);
    print(res.statusCode);
    var preferences = await SharedPreferences.getInstance();
    if (res.statusCode == 200) {
      Map<String, dynamic> jsonresponse = json.decode(res.body);
      String token = jsonresponse['token'];
      await preferences.setString("token", token);
      print('token added ----------------->');
      print(preferences.getString('token'));
      // print(token);
      // Write value
      await storage.write(key: constants.storage_key, value: token);
      String? value = await storage.read(key: constants.storage_key);
      String valuekey = constants.storage_key;
      print(value);
      if (value != '') {
        _showToast("Logged In", Icons.check, Colors.green);
        Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (context) => userHome(
                      tokenkey: valuekey,
                    )));
      }
    } else {
      print("Invalid Credentials");
      _showToast("Invalid Credentials", Icons.close, Colors.red);
    }
  }

  _showToast(toasttext, toasticon, toastcolor) {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: toastcolor,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(toasticon),
          SizedBox(
            width: 12.0,
          ),
          Text(toasttext),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  User user = User('', '', '', '', '', '', '', '', '');
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final double r = (175 / 360);
    final coverHeight = screenWidth * r;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),
      body: Builder(
          builder: (context) => SafeArea(
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: double.infinity,
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(left: 25.0, right: 25.0),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 50,
                          width: double.infinity,
                          alignment: Alignment.center,
                          child: Text(
                            "Sign In to continue",
                            style: TextStyle(
                                fontSize: 27,
                                color: Colors.black,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Form(
                            key: _formKey,
                            child: Column(
                              children: [
                                Container(
                                  child: Material(
                                    shadowColor: Colors.black,
                                    borderRadius: BorderRadius.circular(15.0),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          right: 0.0, left: 0.0),
                                      child: TextFormField(
                                          controller: _emailController,
                                          obscureText: false,
                                          autofocus: false,
                                          keyboardType:
                                              TextInputType.emailAddress,
                                          onChanged: (value) {
                                            user.mobnumber = value;
                                          },
                                          decoration: InputDecoration(
                                            contentPadding:
                                                new EdgeInsets.symmetric(
                                                    vertical: 20.0,
                                                    horizontal: 10.0),
                                            prefixIcon: Icon(
                                              Icons.email,
                                              color: constants.AppMainColor,
                                              size: 25.0, /*Color(0xff224597)*/
                                            ),
                                            labelText: "Mobile Number",
                                            labelStyle: TextStyle(
                                                color: Colors.black,
                                                fontSize: 18.0),
                                            hintText: '',
                                            focusedBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(15.0),
                                              borderSide: BorderSide(
                                                  color: constants.AppMainColor,
                                                  width: 1.5),
                                            ),
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(15.0),
                                              borderSide: BorderSide(
                                                color: constants.AppMainColor,
                                                width: 1.5,
                                              ),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(15.0),
                                              borderSide: BorderSide(
                                                color: Colors.grey,
                                                width: 1.5,
                                              ),
                                            ),
                                          ),
                                          validator: (val) {
                                            if (val!.isEmpty ||
                                                val.length != 10) {
                                              return 'Please enter a valid ' +
                                                  "Mobile Number"
                                                      .toLowerCase() +
                                                  '';
                                            }
                                          }),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 12.0,
                                ),
                                Container(
                                  child: Material(
                                    shadowColor: Colors.black,
                                    borderRadius: BorderRadius.circular(15.0),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          right: 0.0, left: 0.0),
                                      child: TextFormField(
                                          controller: _pwdController,
                                          obscureText: true,
                                          autofocus: false,
                                          keyboardType: TextInputType.text,
                                          onChanged: (value) {
                                            user.password = value;
                                          },
                                          decoration: InputDecoration(
                                            contentPadding:
                                                new EdgeInsets.symmetric(
                                                    vertical: 20.0,
                                                    horizontal: 10.0),
                                            prefixIcon: Icon(
                                              Icons.lock,
                                              color: constants.AppMainColor,
                                              size: 25.0, /*Color(0xff224597)*/
                                            ),
                                            labelText: "Password",
                                            labelStyle: TextStyle(
                                                color: Colors.black,
                                                fontSize: 18.0),
                                            hintText: '',
                                            focusedBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(15.0),
                                              borderSide: BorderSide(
                                                  color: constants.AppMainColor,
                                                  width: 1.5),
                                            ),
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(15.0),
                                              borderSide: BorderSide(
                                                color: constants.AppMainColor,
                                                width: 1.5,
                                              ),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(15.0),
                                              borderSide: BorderSide(
                                                color: Colors.grey,
                                                width: 1.5,
                                              ),
                                            ),
                                          ),
                                          validator: (val) {
                                            if (val!.isEmpty) {
                                              return 'Please enter a valid ' +
                                                  "Password".toLowerCase() +
                                                  '';
                                            }
                                          }),
                                    ),
                                  ),
                                ),
                                // Padding(
                                //   padding: const EdgeInsets.only(
                                //       right: 0.0, top: 10.0),
                                //   child: Align(
                                //       alignment: Alignment.topRight,
                                //       child: Material(
                                //         color: Colors.transparent,
                                //         child: InkWell(
                                //           onTap: () {},
                                //           child: Text(
                                //             "Forgot Password?",
                                //             style: TextStyle(
                                //                 fontSize: 15,
                                //                 fontWeight: FontWeight.bold,
                                //                 color: Colors.grey[700]),
                                //           ),
                                //         ),
                                //       )),
                                // ),
                                SizedBox(
                                  height: 15.0,
                                ),
                                Container(
                                  height: 60.0,
                                  child: ElevatedButton(
                                    onPressed: () async {
                                      if (_formKey.currentState!.validate()) {
                                        print("logged in");
                                        // print("Number = " + user.mobnumber);
                                        // print(user.password);
                                        save();
                                      }
                                      // Navigator.push(
                                      //   context,
                                      //   MaterialPageRoute(
                                      //       builder: (context) => userHome()),
                                      // );
                                    },
                                    style: ElevatedButton.styleFrom(
                                      primary: Colors.white,
                                      elevation: 0.0,
                                      minimumSize: Size(screenWidth, 150),
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 0),
                                      shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(0)),
                                      ),
                                    ),
                                    child: Ink(
                                      decoration: BoxDecoration(
                                          gradient: LinearGradient(
                                              begin: Alignment.centerLeft,
                                              end: Alignment.centerRight,
                                              colors: [
                                                Color(0xF50E276F),
                                                Colors.blueAccent,
                                              ]),
                                          borderRadius:
                                              BorderRadius.circular(10.0)),
                                      child: Container(
                                        alignment: Alignment.center,
                                        child: Text(
                                          "Sign In",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 20),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  height: 50,
                                  // color: Colors.amber,
                                  padding: EdgeInsets.only(left: 10, right: 10),
                                  width: double.infinity,
                                  alignment: Alignment.center,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Expanded(
                                        flex: 4,
                                        child: Container(
                                          height: 1,
                                          color: Colors.grey,
                                        ),
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Container(
                                          alignment: Alignment.center,
                                          child: Text(
                                            "OR",
                                            style: TextStyle(
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600,
                                                color: Colors.grey[700]),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: Container(
                                          height: 1,
                                          color: Colors.grey,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 40,
                                  // color: Colors.amberAccent,
                                  alignment: Alignment.center,
                                  child: TextButton(
                                    child: Text(
                                      "Log in with Email Id",
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500,
                                          color: constants.AppMainColor),
                                    ),
                                    onPressed: () {
                                      print("Email Id Login pressed");
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => userlogin()),
                                      );
                                    },
                                  ),
                                ),
                                Container(
                                  height: 40,
                                  // color: Colors.amberAccent,
                                  alignment: Alignment.center,
                                  child: TextButton(
                                    child: Text(
                                      "Log in with OTP",
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500,
                                          color: constants.AppMainColor),
                                    ),
                                    onPressed: () {
                                      print("OTP Login pressed");
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                userLoginOtp()),
                                      );
                                    },
                                  ),
                                )
                              ],
                            )),
                      ],
                    ),
                  ),
                ),
              )),
      bottomNavigationBar: Container(
        height: 50.0,
        color: Colors.white,
        child: Center(
            child: Wrap(
          children: [
            Text(
              "Don't have an account?  ",
              style: TextStyle(
                  color: Colors.grey[600],
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
            ),
            Material(
                child: InkWell(
              onTap: () {
                print("sign up tapped");
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => userSignup()),
                );
              },
              child: Text(
                "Sign Up",
                style: TextStyle(
                  color: constants.AppMainColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
            )),
          ],
        )),
      ),
    );
  }
}
