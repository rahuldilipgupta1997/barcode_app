import 'dart:convert';
import 'dart:math';

import 'package:FYD/userHome.dart';
import 'package:FYD/userLoginMobile.dart';
import 'package:flutter/material.dart';
import 'constants.dart';
import 'inputTextWidget.dart';
import 'userLogin.dart';
import 'package:http/http.dart' as http;
import 'package:FYD/model/user.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'constants.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class userSignup extends StatefulWidget {
  const userSignup({Key? key}) : super(key: key);
  @override
  _userSignupState createState() => _userSignupState();
}

class _userSignupState extends State<userSignup> {
  final TextEditingController _pwdController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _mobileController = TextEditingController();
  final TextEditingController _firmnameController = TextEditingController();
  final TextEditingController _gstController = TextEditingController();
  final TextEditingController _aadharController = TextEditingController();
  final TextEditingController _addressController = TextEditingController();
  final TextEditingController _upiController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  late FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  Future save() async {
    print("entered");
    var url = Uri.parse(constants.API_URL + 'signup');
    var res = await http.post(url, headers: <String, String>{
      'Context-Type': 'application/json;charSet=UTF-8'
    }, body: <String, String>{
      'Name': user.name,
      'Email': user.email,
      'ContactNumber': user.mobnumber,
      'Password': user.password,
      'Firmname': user.firmname,
      'GSTNo': user.gstno,
      'Aadhar': user.aadhar,
      'Address': user.address,
      'UPI': user.upi,
    });
    print(res.body);
    print(res.statusCode);
    if (res.statusCode == 200) {
      Map<String, dynamic> jsonresponse = json.decode(res.body);
      print(jsonresponse["message"]);
      // String token = jsonresponse['token'];
      if (jsonresponse["message"] == "User already Exist") {
        _showToast("User already Exist", Icons.close, Colors.redAccent);
      } else {
        _showToast(
            "Account Created Successfully. You Will be able to login once Admin Approves.",
            Icons.check,
            Colors.green);
        Navigator.push(context,
            new MaterialPageRoute(builder: (context) => userloginmobile()));
      }
    } else {
      print("Some Error Occurred.Kindly try again later");
      _showToast("Some Error Occurred.Kindly try again later", Icons.refresh,
          Colors.redAccent);
    }
  }

  _showToast(toasttext, toasticon, toastcolor) {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: toastcolor,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
            toasticon,
            color: Colors.white,
          ),
          SizedBox(
            width: 12.0,
          ),
          Expanded(
            child: Text(
              toasttext,
              overflow: TextOverflow.clip,
              softWrap: true,
              maxLines: 2,
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
            ),
          )
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  User user = User('', '', '', '', '', '', '', '', '');

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final double r = (175 / 360);
    final coverHeight = screenWidth * r;
    return Scaffold(
      body: Builder(
        builder: (context) => SafeArea(
          child: SingleChildScrollView(
            child: Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.9,
                alignment: Alignment.center,
                padding: EdgeInsets.only(left: 25.0, right: 25.0),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 50,
                        width: double.infinity,
                        alignment: Alignment.center,
                        child: Text(
                          "Create Account",
                          style: TextStyle(
                              fontSize: 27,
                              color: Colors.black,
                              fontWeight: FontWeight.w700),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              Container(
                                child: Material(
                                  shadowColor: Colors.black,
                                  borderRadius: BorderRadius.circular(15.0),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 0.0, left: 0.0),
                                    child: TextFormField(
                                        controller: _usernameController,
                                        obscureText: false,
                                        autofocus: false,
                                        keyboardType: TextInputType.text,
                                        onChanged: (value) {
                                          user.name = value;
                                        },
                                        decoration: InputDecoration(
                                          contentPadding:
                                              new EdgeInsets.symmetric(
                                                  vertical: 20.0,
                                                  horizontal: 10.0),
                                          prefixIcon: Icon(
                                            Icons.person,
                                            color: constants.AppMainColor,
                                            size: 25.0, /*Color(0xff224597)*/
                                          ),
                                          labelText: "Full Name",
                                          labelStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18.0),
                                          hintText: '',
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                                color: constants.AppMainColor,
                                                width: 1.5),
                                          ),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: constants.AppMainColor,
                                              width: 1.5,
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: Colors.grey,
                                              width: 1.5,
                                            ),
                                          ),
                                        ),
                                        validator: (val) {
                                          if (val!.isEmpty || val.length < 3) {
                                            return 'Please enter a valid ' +
                                                "Name".toLowerCase() +
                                                '';
                                          }
                                        }),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 12.0,
                              ),
                              Container(
                                child: Material(
                                  shadowColor: Colors.black,
                                  borderRadius: BorderRadius.circular(15.0),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 0.0, left: 0.0),
                                    child: TextFormField(
                                        controller: _firmnameController,
                                        obscureText: false,
                                        autofocus: false,
                                        keyboardType: TextInputType.text,
                                        onChanged: (value) {
                                          user.firmname = value;
                                        },
                                        decoration: InputDecoration(
                                          contentPadding:
                                              new EdgeInsets.symmetric(
                                                  vertical: 20.0,
                                                  horizontal: 10.0),
                                          prefixIcon: Icon(
                                            Icons.apartment_outlined,
                                            color: constants.AppMainColor,
                                            size: 25.0, /*Color(0xff224597)*/
                                          ),
                                          labelText: "Name of the Firm",
                                          labelStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18.0),
                                          hintText: '',
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                                color: constants.AppMainColor,
                                                width: 1.5),
                                          ),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: constants.AppMainColor,
                                              width: 1.5,
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: Colors.grey,
                                              width: 1.5,
                                            ),
                                          ),
                                        ),
                                        validator: (val) {
                                          if (val!.isEmpty || val.length < 3) {
                                            return 'Please enter a valid ' +
                                                "Firm Name".toLowerCase() +
                                                '';
                                          }
                                        }),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 12.0,
                              ),
                              Container(
                                child: Material(
                                  shadowColor: Colors.black,
                                  borderRadius: BorderRadius.circular(15.0),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 0.0, left: 0.0),
                                    child: TextFormField(
                                        controller: _mobileController,
                                        obscureText: false,
                                        autofocus: false,
                                        keyboardType:
                                            TextInputType.emailAddress,
                                        onChanged: (value) {
                                          user.mobnumber = value;
                                        },
                                        decoration: InputDecoration(
                                          contentPadding:
                                              new EdgeInsets.symmetric(
                                                  vertical: 20.0,
                                                  horizontal: 10.0),
                                          prefixIcon: Icon(
                                            Icons.pin,
                                            color: constants.AppMainColor,
                                            size: 25.0, /*Color(0xff224597)*/
                                          ),
                                          labelText: "Mobile Number",
                                          labelStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18.0),
                                          hintText: '',
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                                color: constants.AppMainColor,
                                                width: 1.5),
                                          ),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: constants.AppMainColor,
                                              width: 1.5,
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: Colors.grey,
                                              width: 1.5,
                                            ),
                                          ),
                                        ),
                                        validator: (val) {
                                          if (val!.isEmpty ||
                                              val.length != 10) {
                                            return 'Please enter a valid ' +
                                                "Mobile Number".toLowerCase() +
                                                '';
                                          }
                                        }),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 12.0,
                              ),
                              Container(
                                child: Material(
                                  shadowColor: Colors.black,
                                  borderRadius: BorderRadius.circular(15.0),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 0.0, left: 0.0),
                                    child: TextFormField(
                                      controller: _gstController,
                                      obscureText: false,
                                      autofocus: false,
                                      keyboardType: TextInputType.text,
                                      onChanged: (value) {
                                        user.gstno = value;
                                      },
                                      decoration: InputDecoration(
                                        contentPadding:
                                            new EdgeInsets.symmetric(
                                                vertical: 20.0,
                                                horizontal: 10.0),
                                        prefixIcon: Icon(
                                          Icons.pin,
                                          color: constants.AppMainColor,
                                          size: 25.0, /*Color(0xff224597)*/
                                        ),
                                        labelText: "GST Number",
                                        labelStyle: TextStyle(
                                            color: Colors.black,
                                            fontSize: 18.0),
                                        hintText: '',
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                          borderSide: BorderSide(
                                              color: constants.AppMainColor,
                                              width: 1.5),
                                        ),
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                          borderSide: BorderSide(
                                            color: constants.AppMainColor,
                                            width: 1.5,
                                          ),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                          borderSide: BorderSide(
                                            color: Colors.grey,
                                            width: 1.5,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 12.0,
                              ),
                              Container(
                                child: Material(
                                  shadowColor: Colors.black,
                                  borderRadius: BorderRadius.circular(15.0),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 0.0, left: 0.0),
                                    child: TextFormField(
                                        controller: _emailController,
                                        obscureText: false,
                                        autofocus: false,
                                        keyboardType:
                                            TextInputType.emailAddress,
                                        onChanged: (value) {
                                          user.email = value;
                                        },
                                        decoration: InputDecoration(
                                          contentPadding:
                                              new EdgeInsets.symmetric(
                                                  vertical: 20.0,
                                                  horizontal: 10.0),
                                          prefixIcon: Icon(
                                            Icons.email,
                                            color: constants.AppMainColor,
                                            size: 25.0, /*Color(0xff224597)*/
                                          ),
                                          labelText: "Email",
                                          labelStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18.0),
                                          hintText: '',
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                                color: constants.AppMainColor,
                                                width: 1.5),
                                          ),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: constants.AppMainColor,
                                              width: 1.5,
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: Colors.grey,
                                              width: 1.5,
                                            ),
                                          ),
                                        ),
                                        validator: (val) {
                                          if (val!.isEmpty) {
                                            return 'Please enter a valid ' +
                                                "Email".toLowerCase() +
                                                '';
                                          } else if (RegExp(
                                                  r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                              .hasMatch(val)) {
                                            return null;
                                          } else {
                                            return 'Enter valid Email';
                                          }
                                        }),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 12.0,
                              ),
                              Container(
                                child: Material(
                                  shadowColor: Colors.black,
                                  borderRadius: BorderRadius.circular(15.0),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 0.0, left: 0.0),
                                    child: TextFormField(
                                      controller: _aadharController,
                                      obscureText: false,
                                      autofocus: false,
                                      keyboardType: TextInputType.text,
                                      onChanged: (value) {
                                        user.aadhar = value;
                                      },
                                      decoration: InputDecoration(
                                        contentPadding:
                                            new EdgeInsets.symmetric(
                                                vertical: 20.0,
                                                horizontal: 10.0),
                                        prefixIcon: Icon(
                                          Icons.money,
                                          color: constants.AppMainColor,
                                          size: 25.0, /*Color(0xff224597)*/
                                        ),
                                        labelText: "Aadhaar Number",
                                        labelStyle: TextStyle(
                                            color: Colors.black,
                                            fontSize: 18.0),
                                        hintText: '',
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                          borderSide: BorderSide(
                                              color: constants.AppMainColor,
                                              width: 1.5),
                                        ),
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                          borderSide: BorderSide(
                                            color: constants.AppMainColor,
                                            width: 1.5,
                                          ),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                          borderSide: BorderSide(
                                            color: Colors.grey,
                                            width: 1.5,
                                          ),
                                        ),
                                      ),
                                      // validator: (val) {
                                      //   if (val!.isEmpty ||
                                      //       val.length != 12) {
                                      //     return 'Please enter a valid ' +
                                      //         "Aadhaar Number".toLowerCase() +
                                      //         '';
                                      //   }
                                      // }
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 12.0,
                              ),
                              Container(
                                child: Material(
                                  shadowColor: Colors.black,
                                  borderRadius: BorderRadius.circular(15.0),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 0.0, left: 0.0),
                                    child: TextFormField(
                                        controller: _addressController,
                                        obscureText: false,
                                        autofocus: false,
                                        keyboardType: TextInputType.text,
                                        onChanged: (value) {
                                          user.address = value;
                                        },
                                        decoration: InputDecoration(
                                          contentPadding:
                                              new EdgeInsets.symmetric(
                                                  vertical: 20.0,
                                                  horizontal: 10.0),
                                          prefixIcon: Icon(
                                            Icons.home,
                                            color: constants.AppMainColor,
                                            size: 25.0, /*Color(0xff224597)*/
                                          ),
                                          labelText: "Address of the Firm",
                                          labelStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18.0),
                                          hintText: '',
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                                color: constants.AppMainColor,
                                                width: 1.5),
                                          ),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: constants.AppMainColor,
                                              width: 1.5,
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: Colors.grey,
                                              width: 1.5,
                                            ),
                                          ),
                                        ),
                                        validator: (val) {
                                          if (val!.isEmpty || val.length < 3) {
                                            return 'Please enter a valid ' +
                                                "Address".toLowerCase() +
                                                '';
                                          }
                                        }),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 12.0,
                              ),
                              Container(
                                child: Material(
                                  shadowColor: Colors.black,
                                  borderRadius: BorderRadius.circular(15.0),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 0.0, left: 0.0),
                                    child: TextFormField(
                                        controller: _upiController,
                                        obscureText: false,
                                        autofocus: false,
                                        keyboardType: TextInputType.text,
                                        onChanged: (value) {
                                          user.upi = value;
                                        },
                                        decoration: InputDecoration(
                                          contentPadding:
                                              new EdgeInsets.symmetric(
                                                  vertical: 20.0,
                                                  horizontal: 10.0),
                                          prefixIcon: Icon(
                                            Icons.pin,
                                            color: constants.AppMainColor,
                                            size: 25.0, /*Color(0xff224597)*/
                                          ),
                                          labelText: "UPI ID",
                                          labelStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18.0),
                                          hintText: '',
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                                color: constants.AppMainColor,
                                                width: 1.5),
                                          ),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: constants.AppMainColor,
                                              width: 1.5,
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: Colors.grey,
                                              width: 1.5,
                                            ),
                                          ),
                                        ),
                                        validator: (val) {
                                          if (val!.isEmpty || val.length < 5) {
                                            return 'Please enter a valid ' +
                                                "upi id ".toLowerCase() +
                                                '';
                                          }
                                        }),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 12.0,
                              ),
                              Container(
                                child: Material(
                                  shadowColor: Colors.black,
                                  borderRadius: BorderRadius.circular(15.0),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 0.0, left: 0.0),
                                    child: TextFormField(
                                        controller: _pwdController,
                                        obscureText: true,
                                        autofocus: false,
                                        keyboardType: TextInputType.text,
                                        onChanged: (value) {
                                          user.password = value;
                                        },
                                        decoration: InputDecoration(
                                          contentPadding:
                                              new EdgeInsets.symmetric(
                                                  vertical: 20.0,
                                                  horizontal: 10.0),
                                          prefixIcon: Icon(
                                            Icons.lock,
                                            color: constants.AppMainColor,
                                            size: 25.0, /*Color(0xff224597)*/
                                          ),
                                          labelText: "Password",
                                          labelStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18.0),
                                          hintText: '',
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                                color: constants.AppMainColor,
                                                width: 1.5),
                                          ),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: constants.AppMainColor,
                                              width: 1.5,
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: Colors.grey,
                                              width: 1.5,
                                            ),
                                          ),
                                        ),
                                        validator: (val) {
                                          if (val!.isEmpty) {
                                            return 'Please enter a valid ' +
                                                "Password".toLowerCase() +
                                                '';
                                          }
                                        }),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 15.0,
                              ),
                              Container(
                                height: 60.0,
                                child: ElevatedButton(
                                  onPressed: () async {
                                    if (_formKey.currentState!.validate()) {
                                      print("logged in");
                                      print(user.name);
                                      print(user.email);
                                      print(user.mobnumber);
                                      save();
                                    }
                                    // Navigator.push(
                                    //   context,
                                    //   MaterialPageRoute(
                                    //       builder: (context) => userHome(
                                    //           tokenkey: constants.storage_key)),
                                    // );
                                  },
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    elevation: 0.0,
                                    minimumSize: Size(screenWidth, 150),
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 0),
                                    shape: const RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(0)),
                                    ),
                                  ),
                                  child: Ink(
                                    decoration: BoxDecoration(
                                        gradient: constants.appLinerGradient,
                                        borderRadius:
                                            BorderRadius.circular(10.0)),
                                    child: Container(
                                      alignment: Alignment.center,
                                      child: Text(
                                        "Sign Up",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 20),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Container(
                                height: 60.0,
                                width: double.infinity,
                                child: Center(
                                    child: Wrap(
                                  children: [
                                    Text(
                                      "By signing up you agree to our ",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.grey[600],
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15),
                                    ),
                                    Material(
                                        child: InkWell(
                                      onTap: () {
                                        print("sign in tapped");
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  SfPdfViewer.asset(
                                                      'images/terms and conditions.pdf')),
                                        );
                                      },
                                      child: Text(
                                        "Terms and Conditions",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: constants.AppMainColor,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15,
                                        ),
                                      ),
                                    )),
                                  ],
                                )),
                              )
                            ],
                          )),
                    ],
                  ),
                )),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        height: 50.0,
        color: Colors.white,
        child: Center(
            child: Wrap(
          children: [
            Text(
              "Already have an account?  ",
              style: TextStyle(
                  color: Colors.grey[600],
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
            ),
            Material(
                child: InkWell(
              onTap: () {
                print("sign in tapped");
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => userlogin()),
                );
              },
              child: Text(
                "Sign In",
                style: TextStyle(
                  color: constants.AppMainColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
            )),
          ],
        )),
      ),
    );
  }
}
