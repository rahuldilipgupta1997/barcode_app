import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class constants {
  static var API_URL = 'http://45.130.229.159/api/';
  static var PUBLIC_URL = 'http://45.130.229.159/public/';
  // ignore: non_constant_identifier_names
  // static var API_URL = 'http://10.0.2.2:3000/api/';
  // static var PUBLIC_URL = 'http://10.0.2.2:3000/public/';
  // static var API_URL = 'http://localhost:3000';
  //10.0.2.2
  static var AppMainColor = Colors.blueAccent.shade700;
  static var AppsecondaryColor = Colors.blue[400];
  static var storage_key = 'logintoken';
  static var appLinerGradient = LinearGradient(
      begin: Alignment.centerLeft,
      end: Alignment.centerRight,
      colors: [
        Color(0xF50E276F),
        Colors.blueAccent,
      ]);

  static showToast(toasttext, toasticon, toastcolor, fToast) {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: toastcolor,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(toasticon),
          SizedBox(
            width: 12.0,
          ),
          Expanded(
            child: Text(
              toasttext,
              overflow: TextOverflow.clip,
              textAlign: TextAlign.center,
              softWrap: true,
              maxLines: 2,
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
            ),
          )
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }
}
