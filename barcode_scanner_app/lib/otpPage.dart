import 'package:FYD/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:numeric_keyboard/numeric_keyboard.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'userHome.dart';
import 'package:fluttertoast/fluttertoast.dart';
// import 'package:provider/provider.dart';
// import 'package:thegorgeousotp/stores/login_store.dart';
// import 'package:thegorgeousotp/theme.dart';
// import 'package:thegorgeousotp/widgets/loader_hud.dart';
import 'constants.dart';

class OtpPage extends StatefulWidget {
  final String phone_no;

  const OtpPage({Key? key, required this.phone_no}) : super(key: key);
  @override
  _OtpPageState createState() => _OtpPageState();
}

class _OtpPageState extends State<OtpPage> {
  String text = '';
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  late FToast fToast;
  final storage = new FlutterSecureStorage();

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  void _onKeyboardTap(String value) {
    setState(() {
      if (text.length < 6) {
        text = text + value;
      }
    });
    print(text);
  }

  Widget otpNumberWidget(int position) {
    try {
      return InkWell(
        onTap: () {
          print("tapped");
        },
        child: Container(
          height: 40,
          width: 40,
          decoration: BoxDecoration(
              border: Border.all(color: Colors.black, width: 0),
              borderRadius: const BorderRadius.all(Radius.circular(8))),
          child: Center(
              child: Text(
            text[position],
            style: TextStyle(color: Colors.black),
          )),
        ),
      );
    } catch (e) {
      return Container(
        height: 40,
        width: 40,
        decoration: BoxDecoration(
            border: Border.all(color: Colors.black, width: 0),
            borderRadius: const BorderRadius.all(Radius.circular(8))),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      key: _scaffoldKey,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                            margin: const EdgeInsets.symmetric(horizontal: 20),
                            child: Text(
                                'Enter 6 digits verification code sent to your number',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 26,
                                    fontWeight: FontWeight.w500))),
                        Container(
                          constraints: const BoxConstraints(maxWidth: 500),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              otpNumberWidget(0),
                              otpNumberWidget(1),
                              otpNumberWidget(2),
                              otpNumberWidget(3),
                              otpNumberWidget(4),
                              otpNumberWidget(5),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 10),
                    constraints: const BoxConstraints(maxWidth: 500),
                    child: RaisedButton(
                      onPressed: () async {
                        if (text.length == 6) {
                          var url = Uri.parse(constants.API_URL + 'verify_otp');
                          var res =
                              await http.post(url, headers: <String, String>{
                            'Context-Type': 'application/json;charSet=UTF-8'
                          }, body: <String, String>{
                            'otp': text,
                            'phone': widget.phone_no,
                          });
                          print(res.body);
                          print(res.statusCode);
                          if (res.statusCode == 200) {
                            Map<String, dynamic> jsonresponse =
                                json.decode(res.body);
                            String result =
                                json.decode(jsonresponse['result'])['result'];
                            if (result == 'success') {
                              String token = jsonresponse['token'];
                              await storage.write(
                                  key: constants.storage_key, value: token);
                              String? value = await storage.read(
                                  key: constants.storage_key);
                              String valuekey = 'lokintoken';
                              // print(value);
                              if (value != '') {
                                var preferences =
                                    await SharedPreferences.getInstance();
                                await preferences.setString("token", token);
                                print('token added ----------------->');
                                print(preferences.getString('token'));
                                constants.showToast("Logged In", Icons.check,
                                    Colors.green, fToast);
                                Navigator.push(
                                    context,
                                    new MaterialPageRoute(
                                        builder: (context) => userHome(
                                              tokenkey: valuekey,
                                            )));
                              }
                            } else {
                              print("Invalid code");
                              constants.showToast("Invalid code", Icons.close,
                                  Colors.red, fToast);
                            }
                          } else {
                            constants.showToast("Something went wrong",
                                Icons.close, Colors.red, fToast);
                            //error
                          }
                        } else {
                          constants.showToast("Enter 6 digit otp", Icons.close,
                              Colors.red, fToast);
                          //error
                        }
                        // loginStore.validateOtpAndLogin(context, text);
                      },
                      color: constants.AppMainColor,
                      shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(14))),
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Confirm',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 18),
                            ),
                            Container(
                              padding: const EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(20)),
                                color: constants.AppsecondaryColor,
                              ),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white,
                                size: 16,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: NumericKeyboard(
                      onKeyboardTap: _onKeyboardTap,
                      textColor: Colors.black,
                      rightIcon: Icon(
                        Icons.backspace,
                        color: constants.AppsecondaryColor,
                      ),
                      rightButtonFn: () {
                        setState(() {
                          text = text.substring(0, text.length - 1);
                        });
                      },
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
