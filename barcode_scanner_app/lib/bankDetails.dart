import 'dart:convert';

import 'package:FYD/userHome.dart';
import 'package:FYD/userProfile.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'constants.dart';
import 'package:new_gradient_app_bar/new_gradient_app_bar.dart';
import 'package:FYD/model/bankdetails.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'userHome.dart';
import 'editBankDetails.dart';
import 'userLogin.dart';

class bankDetails extends StatefulWidget {
  const bankDetails({Key? key}) : super(key: key);
  @override
  _bankDetailsState createState() => _bankDetailsState();
}

class _bankDetailsState extends State<bankDetails> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _accnoController = TextEditingController();
  final TextEditingController _ifscController = TextEditingController();
  final TextEditingController _banknameController = TextEditingController();
  final TextEditingController _confirmaccnoController = TextEditingController();
  final TextEditingController _branchController = TextEditingController();
  final storage = new FlutterSecureStorage();
  late FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  Future save() async {
    String? value = await storage.read(key: constants.storage_key);
    print("token = " + value!);
    Map<String, dynamic> decodedToken = JwtDecoder.decode(value);
    print(decodedToken["_id"]);
    var url = Uri.parse(constants.API_URL + 'bank-details');
    print(url);
    print(bankdetails.AccNumber);
    var res = await http.post(url, headers: <String, String>{
      'Context-Type': 'application/json;charSet=UTF-8',
      'authorization': value
    }, body: <String, String>{
      "UserID": decodedToken["_id"],
      "AccountNo": bankdetails.AccNumber,
      "Confirmaccno": bankdetails.Confirmaccno,
      "BankName": bankdetails.BankName,
      "Branch": bankdetails.Branch,
      "IFSC": bankdetails.IFSC
    });
    print(res.body);
    print(res.statusCode);
    if (res.statusCode == 200) {
      _showToast("Bank Account Added Successfully", Icons.check, Colors.green);
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => userHome(
            tokenkey: constants.storage_key,
          ),
        ),
      );
    } else {
      print("Failed to add ");
      _showToast("Failed to Add Bank Account", Icons.close, Colors.red);
    }
  }

  _showToast(toasttext, toasticon, toastcolor) {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: toastcolor,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(toasticon),
          SizedBox(
            width: 12.0,
          ),
          Expanded(
            child: Text(
              toasttext,
              overflow: TextOverflow.clip,
              textAlign: TextAlign.center,
              softWrap: true,
              maxLines: 2,
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
            ),
          )
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.TOP,
      toastDuration: Duration(seconds: 2),
    );
  }

  Bankdetails bankdetails = new Bankdetails("", "", "", "", "", "");
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final double r = (175 / 360);
    final coverHeight = screenWidth * r;

    return WillPopScope(
        onWillPop: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => userHome(
                      tokenkey: constants.storage_key,
                    )),
          );
          return Future.value(true);
        },
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: NewGradientAppBar(
              automaticallyImplyLeading: true,
              gradient: constants.appLinerGradient,
              title: Transform(
                transform: Matrix4.translationValues(0.0, 0.0, 0.0),
                child: Text(
                  'Bank Details',
                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w700),
                ),
              ),
              actions: [
                Padding(
                  padding: EdgeInsets.only(right: 10),
                  child: PopupMenuButton(
                    // offset: Offset(0, 50),
                    child: Center(
                      child: Icon(Icons.more_vert_outlined),
                    ),
                    itemBuilder: (context) => [
                      PopupMenuItem(
                          child: TextButton(
                        child: Text(
                          'Logout',
                          style: TextStyle(color: constants.AppMainColor),
                        ),
                        onPressed: () async {
                          print("Logout Clicked");
                          await storage.delete(key: constants.storage_key);
                          String? value =
                              await storage.read(key: constants.storage_key);
                          print(value);
                          print("Delete the sharepreference data---------->");
                          var preferences =
                              await SharedPreferences.getInstance();
                          await preferences.clear();
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => userlogin()));
                        },
                      )),
                    ],
                  ),
                )
              ]),
          body: Builder(
            builder: (context) => SafeArea(
              child: Container(
                padding: EdgeInsets.all(20),
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              Container(
                                child: Material(
                                  shadowColor: Colors.black,
                                  borderRadius: BorderRadius.circular(15.0),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 0.0, left: 0.0),
                                    child: TextFormField(
                                        controller: _accnoController,
                                        obscureText: false,
                                        autofocus: false,
                                        keyboardType: TextInputType.number,
                                        onChanged: (value) {
                                          bankdetails.AccNumber = value;
                                        },
                                        decoration: InputDecoration(
                                          contentPadding:
                                              new EdgeInsets.symmetric(
                                                  vertical: 20.0,
                                                  horizontal: 25.0),
                                          labelText: "Account Number",
                                          labelStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18.0),
                                          hintText: '',
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                                color: constants.AppMainColor,
                                                width: 1.5),
                                          ),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: constants.AppMainColor,
                                              width: 1.5,
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: Colors.grey,
                                              width: 1.5,
                                            ),
                                          ),
                                        ),
                                        validator: (val) {
                                          if (val!.isEmpty) {
                                            return 'Please enter a valid ' +
                                                "Account Number";
                                          }
                                        }),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 12,
                              ),
                              Container(
                                child: Material(
                                  shadowColor: Colors.black,
                                  borderRadius: BorderRadius.circular(15.0),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 0.0, left: 0.0),
                                    child: TextFormField(
                                        controller: _confirmaccnoController,
                                        obscureText: false,
                                        autofocus: false,
                                        keyboardType: TextInputType.number,
                                        onChanged: (value) {
                                          bankdetails.Confirmaccno = value;
                                        },
                                        decoration: InputDecoration(
                                          contentPadding:
                                              new EdgeInsets.symmetric(
                                                  vertical: 20.0,
                                                  horizontal: 25.0),
                                          labelText: "Confirm Account Number",
                                          labelStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18.0),
                                          hintText: '',
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                                color: constants.AppMainColor,
                                                width: 1.5),
                                          ),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: constants.AppMainColor,
                                              width: 1.5,
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: Colors.grey,
                                              width: 1.5,
                                            ),
                                          ),
                                        ),
                                        validator: (val) {
                                          if (val!.isEmpty) {
                                            return 'Please enter a valid ' +
                                                "Account Number";
                                          }
                                        }),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 12.0,
                              ),
                              Container(
                                child: Material(
                                  shadowColor: Colors.black,
                                  borderRadius: BorderRadius.circular(15.0),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 0.0, left: 0.0),
                                    child: TextFormField(
                                        controller: _banknameController,
                                        obscureText: false,
                                        autofocus: false,
                                        keyboardType: TextInputType.text,
                                        onChanged: (value) {
                                          bankdetails.BankName = value;
                                        },
                                        decoration: InputDecoration(
                                          contentPadding:
                                              new EdgeInsets.symmetric(
                                                  vertical: 20.0,
                                                  horizontal: 25.0),
                                          labelText: "Bank Name",
                                          labelStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18.0),
                                          hintText: '',
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                                color: constants.AppMainColor,
                                                width: 1.5),
                                          ),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: constants.AppMainColor,
                                              width: 1.5,
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: Colors.grey,
                                              width: 1.5,
                                            ),
                                          ),
                                        ),
                                        validator: (val) {
                                          if (val!.isEmpty) {
                                            return 'Please enter a valid ' +
                                                "Bank Name";
                                          }
                                        }),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 12.0,
                              ),
                              Container(
                                child: Material(
                                  shadowColor: Colors.black,
                                  borderRadius: BorderRadius.circular(15.0),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 0.0, left: 0.0),
                                    child: TextFormField(
                                        controller: _branchController,
                                        obscureText: false,
                                        autofocus: false,
                                        keyboardType: TextInputType.text,
                                        onChanged: (value) {
                                          bankdetails.Branch = value;
                                        },
                                        decoration: InputDecoration(
                                          contentPadding:
                                              new EdgeInsets.symmetric(
                                                  vertical: 20.0,
                                                  horizontal: 25.0),
                                          labelText: "Branch",
                                          labelStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18.0),
                                          hintText: '',
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                                color: constants.AppMainColor,
                                                width: 1.5),
                                          ),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: constants.AppMainColor,
                                              width: 1.5,
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: Colors.grey,
                                              width: 1.5,
                                            ),
                                          ),
                                        ),
                                        validator: (val) {
                                          if (val!.isEmpty) {
                                            return 'Please enter a valid ' +
                                                "Branch";
                                          }
                                        }),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 12,
                              ),
                              Container(
                                child: Material(
                                  shadowColor: Colors.black,
                                  borderRadius: BorderRadius.circular(15.0),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 0.0, left: 0.0),
                                    child: TextFormField(
                                        controller: _ifscController,
                                        obscureText: false,
                                        autofocus: false,
                                        keyboardType: TextInputType.text,
                                        onChanged: (value) {
                                          bankdetails.IFSC = value;
                                        },
                                        decoration: InputDecoration(
                                          contentPadding:
                                              new EdgeInsets.symmetric(
                                                  vertical: 20.0,
                                                  horizontal: 25.0),
                                          labelText: "IFSC",
                                          labelStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18.0),
                                          hintText: '',
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                                color: constants.AppMainColor,
                                                width: 1.5),
                                          ),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: constants.AppMainColor,
                                              width: 1.5,
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            borderSide: BorderSide(
                                              color: Colors.grey,
                                              width: 1.5,
                                            ),
                                          ),
                                        ),
                                        validator: (val) {
                                          if (val!.isEmpty) {
                                            return 'Please enter a valid IFSC';
                                          }
                                        }),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 15.0,
                              ),
                              Container(
                                height: 60.0,
                                child: ElevatedButton(
                                  onPressed: () async {
                                    if (_formKey.currentState!.validate()) {
                                      print("Adding Bank Details");
                                      if (_accnoController.text ==
                                          _confirmaccnoController.text) {
                                        print("equal");
                                        save();
                                      } else {
                                        _showToast(
                                            "Account Number doesn't matches with Confirm Account Number",
                                            Icons.close,
                                            Colors.red);
                                      }
                                    }
                                    // Navigator.push(
                                    //   context,
                                    //   MaterialPageRoute(
                                    //       builder: (context) => userHome()),
                                    // );
                                  },
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    elevation: 0.0,
                                    minimumSize: Size(screenWidth, 150),
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 0),
                                    shape: const RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(0)),
                                    ),
                                  ),
                                  child: Ink(
                                    decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                            begin: Alignment.centerLeft,
                                            end: Alignment.centerRight,
                                            colors: [
                                              Color(0xF50E276F),
                                              Colors.blueAccent,
                                            ]),
                                        borderRadius:
                                            BorderRadius.circular(10.0)),
                                    child: Container(
                                      alignment: Alignment.center,
                                      child: Text(
                                        "Submit",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 20),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}
