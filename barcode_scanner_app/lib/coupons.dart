import 'package:flutter/material.dart';
import 'constants.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_gradient_colors/flutter_gradient_colors.dart';
import 'dart:convert';

class coupons extends StatefulWidget {
  coupons({Key? key}) : super(key: key);
  @override
  _couponsState createState() => _couponsState();
}

class _couponsState extends State<coupons> {
  final storage = new FlutterSecureStorage();
  var coupons;
  @override
  void initState() {
    super.initState();
    coupons = _fetchCoupons();
  }

  Future<String> getUserId() async {
    String? value = await storage.read(key: constants.storage_key);
    String valuekey = value!;
    print(valuekey);
    Map<String, dynamic> decodedToken = JwtDecoder.decode(valuekey);
    print(decodedToken["_id"]);
    print(decodedToken);
    return decodedToken["_id"].toString();
  }

  Future<List> _fetchCoupons() async {
    try {
      String userId = await getUserId();
      String? token = await storage.read(key: constants.storage_key);
      print("token=" + token!);
      var url = Uri.parse(constants.API_URL + 'getcoupons/' + userId);
      var res = await http.get(url, headers: <String, String>{
        'Context-Type': 'application/json;charSet=UTF-8',
        'authorization': token
      });
      print(jsonDecode(res.body));
      print(res.statusCode);
      if (res.statusCode == 200) {
        return jsonDecode(res.body)['data'];
      } else {
        return [];
      }
    } catch (e) {
      print(e);
      return [];
    }
  }

  Widget _listView(AsyncSnapshot<List> snapshot) {
    if (snapshot.connectionState == ConnectionState.waiting) {
      return Column(children: [
        Padding(
          padding: EdgeInsets.only(top: 16),
        ),
        Center(
          child: SizedBox(
            child: CircularProgressIndicator(),
            width: 60,
            height: 60,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 16),
          child: Text('Loading...'),
        )
      ]);
    }

    if (snapshot.data!.length > 0) {
      return ListView(
          children: snapshot.data!.map((var data) {
        print(data);

        return Container(
          height: 140,
          width: MediaQuery.of(context).size.width,
          // padding: EdgeInsets.all(10),
          margin: EdgeInsets.only(top: 5),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            // gradient: constants.appLinerGradient
            gradient: LinearGradient(
              colors: GradientColors.lightBlack,
              begin: const FractionalOffset(0.0, 0.0),
              end: const FractionalOffset(1.0, 0.0),
              stops: [0.0, 1.0],
              tileMode: TileMode.clamp,
            ),
            // color: Colors.amber
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 50,
                padding: EdgeInsets.only(left: 20, right: 20, top: 10),
                // color: Colors.red,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      data['coupon']['Amount'].toString() + " ₹",
                      style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.w700,
                          color: Colors.white),
                    ),
                    Text(data['coupon']['CouponNumber'].toString(),
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.w700,
                            color: Colors.greenAccent.shade400))
                  ],
                ),
              ),
              Container(
                height: 40,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    RotatedBox(
                      quarterTurns: 1,
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30),
                          ),
                          color: Colors.white,
                        ),
                        width: 60,
                        height: 25,
                      ),
                    ),
                    RotatedBox(
                      quarterTurns: 3,
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30),
                          ),
                          color: Colors.white,
                        ),
                        width: 60,
                        height: 25,
                      ),
                    )
                  ],
                ),
              ),
              Container(
                height: 50,
                padding: EdgeInsets.only(left: 20, right: 20, bottom: 10),
                // color: Colors.red,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    // Text(
                    //     "Redeemed on " +
                    //         data['coupon']['RedemptionDate']
                    //             .toString()
                    //             .split("T")[0],
                    //     style: TextStyle(
                    //         fontSize: 19,
                    //         fontWeight: FontWeight.w700,
                    //         color: Colors.grey.shade400)),
                    Text(
                        "Expires on " +
                            data['coupon']['ExpiryDate']
                                .toString()
                                .split("T")[0],
                        style: TextStyle(
                            fontSize: 19,
                            fontWeight: FontWeight.w700,
                            color: Colors.white))
                  ],
                ),
              ),
            ],
          ),
        );
      }).toList());
    } else {
      return Center(
        child: Text(
          "No Coupons Earned yet.",
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(
        builder: (context) => SafeArea(
          child: Container(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
            // child: Expanded(
            child: FutureBuilder(
                future:
                    coupons!, // a previously-obtained Future<String> or null
                builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                  return _listView(snapshot);
                }),
          ),
        ),
      ),
    );
  }
}
